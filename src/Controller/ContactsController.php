<?php

namespace App\Controller;

use App\Services\RestAPIServices\Contacts\SynchronizeContacts;
use App\Services\RestAPIServices\Contacts\UnsubscribeContacts;
use App\Services\RestAPIServices\Formatter\XMLSyncContactsListFormatter;
use App\Services\ToolServices;
use Exception;
use JsonException;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use App\Services\MioServices;

class ContactsController extends AbstractController
{
    protected $hasPermission = true;
    protected $mioService;
    protected $headers;

    public function __construct(MioServices $mioService, ToolServices $toolServices)
    {
        $this->mioService = $mioService;
        $this->headers = $toolServices->setHeadersToAvoidCORSBlocking();
    }

    /**
     * @Route("/customfields/get", name="get_custom_fields",  methods={"POST"})
     * @param Request $request
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function getCustomFields(Request $request, LoggerInterface $logger): JsonResponse
    {
        $return_array = [];
        $records = [];
        $err = '';
        $apikey = $request->get('apikey');

        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_OK, $this->headers);
        }

        if ($this->hasPermission) {
            $return_array = $this->mioService->getContactsCustomFields($apikey);
            //return $this->json($return_array, Response::HTTP_OK, $this->headers);
            return $this->json($return_array, Response::HTTP_OK, $this->headers);
        } else {
            return $this->json(['error' => 'not authorized'], Response::HTTP_UNAUTHORIZED, $this->headers);
        }
    }


    /**
     *
     * @Route("/standardfields/get", name="get_standard_fields",  methods={"POST"})
     * @return JsonResponse
     */
    public function getStandardFields(): JsonResponse
    {
        if ($this->hasPermission) {
            $return_array = $this->mioService->getContactsStandardFields();
            return $this->json($return_array, Response::HTTP_OK, $this->headers);
        } else {
            return $this->json(['error' => 'not authorized'], Response::HTTP_UNAUTHORIZED, $this->headers);
        }
    }

    /**
     * @Route("/customfield/sync", name="customfield.sync", methods={"POST"})
     * @param Request $request
     * @param LoggerInterface $logger
     * @param MioServices $mioServices
     * @return JsonResponse|null
     * @author Pradeep
     */
    public function syncCustomFields(Request $request, LoggerInterface $logger, MioServices $mioServices): ?JsonResponse
    {
        try {
            $encodedData = $request->getContent();
            if (empty($encodedData)) {
                throw new RuntimeException('Invalid Request');
            }
            $requestArr = json_decode(base64_decode($encodedData), true, 512, JSON_THROW_ON_ERROR);
            // validate and setAPIKey for mioServices.
            if (!isset($requestArr[ 'apikey' ], $requestArr[ 'customfieldlist' ]) ||
                !$mioServices->setApiKey($requestArr[ 'apikey' ])) {
                throw new RuntimeException('Invalid APIKey or CustomFieldList provided');
            }
            $logger->info(json_encode(['request' => $requestArr]));
            $eMIOCustomFields = $mioServices->getContactsCustomFields();
            foreach ($requestArr[ 'customfieldlist' ] as $customField) {
                // Dont create, when custom field is already present in eMIO.
                if ($customField[ 'operation' ] !== 'delete' && array_key_exists($customField[ 'fieldname' ],
                        $eMIOCustomFields)) {
                    continue;
                }
                $logger->info(json_encode($customField));
                $datatype = $customField[ 'datatype' ] ?? '';
                $status = $mioServices->synchronizeCustomFields($customField[ 'fieldname' ],
                    $customField[ 'operation' ], $datatype);
                if ($status === false) {
                    throw new RuntimeException('Failed to insert or unsetEmailList custom field ' . $customField[ 'fieldname' ] . ' in eMIO');
                }
            }
            return $this->json('Successfully inserted or updated Custom Fields to eMIO', Response::HTTP_OK, $this->headers);
        } catch (Exception | RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST, $this->headers);
        }
    }

    /**
     * @Route("/customfield/delete", name="customfield.delete", methods={"POST"})
     * @param Request $request
     * @param MioServices $mioServices
     * @param LoggerInterface $logger
     * @return JsonResponse|null
     * @author Pradeep
     */
    public function deleteCustomField(
        Request $request,
        MioServices $mioServices,
        LoggerInterface $logger
    ): ?JsonResponse {
        $encodedData = $request->getContent();
        try {
            if (empty($encodedData)) {
                throw new RuntimeException('Invalid request');
            }
            $requestArr = json_decode(base64_decode($encodedData), true, 512,
                JSON_THROW_ON_ERROR);// validate and setAPIKey for mioServices.
            if (!isset($requestArr[ 'apikey' ], $requestArr[ 'customfieldlist' ]) ||
                !$mioServices->setApiKey($requestArr[ 'apikey' ])) {
                throw new RuntimeException('Invalid APIKey or CustomFieldList provided');
            }
            $eMIOCustomFields = $mioServices->getContactsCustomFields();
            foreach ($requestArr[ 'customfieldlist' ] as $customField) {
                // customfield already present in eMIO
                if (!array_key_exists($customField[ 'fieldname' ], $eMIOCustomFields)) {
                    continue;
                }
                $status = $mioServices->deleteCustomField($customField[ 'fieldname' ]);
                if ($status === false) {
                    throw new RuntimeException('Failed to insert or unsetEmailList customfield ' . $customField[ 'fieldname' ] . ' in eMIO');
                }
            }
            return $this->json('Successfully delete CustomField', Response::HTTP_OK, $this->headers);
        } catch (Exception | RuntimeException $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST, $this->headers);
        }
    }

    /**
     * @Route("/customfield/create", name="customfield.create", methods={"POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param MioServices $mioServices
     * @param LoggerInterface $logger
     * @return Response|null
     * @author Pradeep
     */
    public function createCustomField(
        Request $request,
        ToolServices $toolServices,
        MioServices $mioServices,
        LoggerInterface $logger
    ): ?JsonResponse {
        $encodedData = $request->getContent();
        $content = $toolServices->unpackContent($encodedData);
        $logger->info('createCustomFeild ' . json_encode($content, JSON_THROW_ON_ERROR));
        if (!isset($content[ 'apikey' ], $content[ 'customfield' ]) || empty($content[ 'apikey' ]) ||
            !$mioServices->setApiKey($content[ 'apikey' ]))  {
            throw new RuntimeException('Invalid Request provided');
        }
        $status = $mioServices->createCustomFields($content[ 'customfield' ], $content['append_mio_tag']);
        return $this->json(['status'=>$status], Response::HTTP_OK, $this->headers);
    }

    /**
     * @Route("/contacts/sync", name="contacts_sync",  methods={"POST","GET"})
     */
    public function sync(
        Request $request,
        SynchronizeContacts $synchronizeContacts,
        ToolServices $toolServices
    ) {
        $content = $request->getContent();
        $data = $toolServices->unpackContent($content, true);
        if(!isset($data['contacts'], $data['apikey'])){
            $this->json(['status'=>'Missing Contacts or APIKey'], Response::HTTP_BAD_REQUEST, $this->headers);
        }
        $contacts = $data['contacts'];
        $apikey = $data['apikey'];
        $status = $synchronizeContacts->sync($apikey, $contacts);
        return $this->json(['status'=>$status], Response::HTTP_OK, $this->headers);
    }

    /**
     * @Route("/contact/unsubscribe", name="contact_unsubscribe", methods={"POST"})
     * @param Request $request
     * @param UnsubscribeContacts $unsubscribeContacts
     * @return JsonResponse
     * @throws \JsonException
     * @author Pradeep
     * @internal Documentation of Service https://maileon.com/support/unsubscribe-contacts-by-email/
     */
    public function unsubscribe(
        Request $request,
        UnsubscribeContacts $unsubscribeContacts,
        ToolServices $toolServices,
        LoggerInterface $logger
    ):JsonResponse
    {
        $encodedData = $request->getContent();
        try {
            $data = $toolServices->unpackContent($encodedData);
            if (!isset($data[ 'email' ], $data[ 'apikey' ])) {
                $this->json(['status' => 'Missing Contacts or APIKey'], Response::HTTP_BAD_REQUEST, $this->headers);
            }
            $email = $data[ 'email' ];
            $apikey = $data[ 'apikey' ];
            $logger->info("Unsubscribe " . json_encode($data));
            $statusCode = $unsubscribeContacts->unsubscribe($apikey, $email);
            $logger->info("return status code ".$statusCode);
            // return value of unsubscribe service is 204
            if ($statusCode !== Response::HTTP_NO_CONTENT) {
                $message = "failed to unsubscribe email address";
            } else {
                $message = "successfully unsubscribed email address";
            }
            return $this->json($message, $statusCode, $this->headers);
        } catch (JsonException $e) {
            $logger->error($e->getMessage(),[__METHOD__, __LINE__]);
            return $this->json($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR, $this->headers);
        }
    }
}


