<?php


namespace App\Controller;


use App\Services\MioServices;
use App\Services\RestAPIServices\Blacklists\Blacklist;
use App\Services\ToolServices;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlacklistController extends AbstractController
{

	protected $headers;

	public function __construct(ToolServices $toolServices)
	{
		$this->headers = $toolServices->setHeadersToAvoidCORSBlocking();
	}


	/**
     * @Route("/blacklist/get", methods={"GET","POST"})
     * @param Request $request
     * @param MioServices $mioServices
     * @return JsonResponse
     * @author Pradeep
     */
    public function getBlacklistAction(Request $request, MioServices $mioServices):JsonResponse
    {
        $apikey = $request->get('apikey');
        if(empty($apikey) || !$mioServices->setApiKey($apikey)){
            return $this->json(['error' => 'Invalid APIKey'], Response::HTTP_BAD_REQUEST);
        }
        return $this->json($mioServices->getBlacklist(), Response::HTTP_OK, $this->headers);
    }


    /**
     * @Route("/blacklist/emails", name="getBlacklists", methods={"GET","POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param MioServices $mioServices
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @author Pradeep
     */
    public function postEntriesToBlacklistAction(Request $request, ToolServices $toolServices, MioServices $mioServices, LoggerInterface $logger) :JsonResponse
    {
        $content = $request->getContent();
        $data = $toolServices->unpackContent($content);
        if(!isset($data[ 'blk_email_list' ], $data[ 'blk_id' ], $data[ 'api_key' ]) || empty($data[ 'api_key' ])) {
            return $this->json(['error' => 'Invalid configuration provided'], Response::HTTP_BAD_REQUEST, $this->headers);
        }
        $blacklist_email = $data[ 'blk_email_list' ];
        $blacklist_id = $data[ 'blk_id' ];
        $api_key = $data[ 'api_key' ];
        $blacklist_import_name = $data[ 'blk_imp_name' ];
        if (empty($blacklist_email) || empty($api_key) || !$mioServices->setApiKey($api_key)) {
            $logger->info('no api_key or blacklist email', [__METHOD__, __LINE__]);
            return $this->json(['error' => 'no api_key or blacklist Emails'], Response::HTTP_BAD_REQUEST, $this->headers);
        }
        return $this->json($mioServices->blackListEmail($blacklist_id, $blacklist_email, $blacklist_import_name), Response::HTTP_OK, $this->headers);
    }

    /**
     * @Route("/v2.0/blacklist", name="blacklistEmailsUsingRestAPI", methods={"GET", "POST"})
     * @param Request $request
     * @param ToolServices $toolServices
     * @param Blacklist $restApiServices
     * @return JsonResponse
     * @author Pradeep
     */
    public function blacklistEmailsUsingRestAPI(
        Request $request,
        ToolServices $toolServices,
        LoggerInterface $logger,
        Blacklist $restApiServices
    )
    {
        $content = $request->getContent();
        if(empty($content)) {
            return $this->json('Bad request payload sent');
        }

        $data = $toolServices->unpackContent($content);
        if(!isset($data['api_key'], $data['blk_imp_name'], $data['blk_id'],$data['blk_email_list'])) {
            return $this->json('Invalid request to blacklist emails.',Response::HTTP_BAD_REQUEST, $this->headers);
        }

        if (empty($data['api_key']) || empty($data['blk_id']) ){
            return $this->json('missing apikey or blacklist id.',Response::HTTP_BAD_REQUEST, $this->headers);
        }
        $emails = json_decode($data[ 'blk_email_list' ], true, 512, JSON_THROW_ON_ERROR);
        if(empty($emails)) {
            return $this->json("missing emails to blacklist.",Response::HTTP_BAD_REQUEST, $this->headers);
        }
        $apikey = $data['api_key'] ;
        $importName =$data['blk_imp_name'];
        $blkId = $data['blk_id'];
        $status = $restApiServices->AddEntriesToBlacklist($apikey, $blkId, $importName, $emails);
        return $this->json($status,Response::HTTP_OK, $this->headers);
    }
}
