<?php


namespace App\Controller;

use App\Datatypes\MioReturnType;
use App\Services\MioServices;
use App\Services\ToolServices;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class CioController extends AbstractController
{
    protected $mioService;
    protected $logger;
    protected $headers;

    public function __construct(MioServices $mioService, LoggerInterface $logger, ToolServices $toolsService)
    {
        $this->mioService = $mioService;
        $this->logger = $logger;
        $this->headers = $toolsService->setHeadersToAvoidCORSBlocking();
    }


    /**
     * @Route("/check", name="check",  methods={"GET"})
     * @return JsonResponse
     */
    public function check(): JsonResponse
    {
        return $this->json(['ckeck' => 'done'], 200, $this->headers);
    }


	/**
	 * @Route("/run/marketingautomation", name="run.marketinautomation",  methods={"POST"})
	 * @param Request $request
	 */
	public function runMarketingAutomation(Request $request, MioServices $mioServices)
	{
		$data = json_decode(base64_decode($request->getContent()), true, 512, JSON_OBJECT_AS_ARRAY);

		$mioServices->setApiKey($data['apikey']);
		$ret = $mioServices->processhookRunMA($data['email'], $data['ma_id']);
		return $this->json($ret, 200, $this->headers);
	}


    /**
     * @param Request $request
     * @Route("/get_mio_data", name="get_mio_data",  methods={"POST"})
     */
    public function getMioData(Request $request): JsonResponse
    {

        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST);
        }
        $this->mioService->setApiKey($apikey);
        $data[ MioServices::INSTRUCTIONS ] = $this->mioService->getInstructionFields(); // fast
        $data[ MioServices::INTEGRATIONTYPESMIO ] = $this->mioService->getIntegrationTypes(); // fast
        // Todo modified function getContactsStandardFields , need to check the further execution
        $data[ MioServices::STANDARDFIELDS ] = $this->mioService->getContactsStandardFields(); // fast
        $data[ MioServices::CUSTOMFIELDS ] = $this->mioService->getContactsCustomFields($apikey); // MIO call
        $data[ MioServices::CONTACTEVENTFIELDS ] = $this->mioService->getContactEventFields(); // fast
        $t1 = microtime(true);
        $data[ MioServices::DOIKEYLIST ] = $this->mioService->getMailingsDoiKeys($apikey); // MIO call
        $data[ MioServices::CONTACTEVENTSLIST ] = $this->mioService->getTransactionContactEvents($apikey); // Both MarketingAutomation & ContactEvents, MIO call
        $t2 = microtime(true);
        $t = $t2 - $t1;
        $this->logger->info("RUNTIME MIO = {$t}", [__METHOD__, __LINE__]);
        return $this->json($data, Response::HTTP_OK, $this->headers);
    }


    /**
     * @Route("/getMioBaseData", name="getMioBaseData",  methods={"GET"})
     */
    public function getMioBaseData(Request $request): JsonResponse
    {
        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST);
        }
        $this->mioService->setApiKey($apikey);
        $data[ MioServices::INSTRUCTIONS ] = $this->mioService->getInstructionFields(); // fast
        $data[ MioServices::INTEGRATIONTYPESMIO ] = $this->mioService->getIntegrationTypes(); // fast
        $data[ MioServices::STANDARDFIELDS ] = $this->mioService->getContactsStandardFields(); // fast
        $data[ MioServices::CONTACTEVENTFIELDS ] = $this->mioService->getContactsCustomFields(); // fast
        return $this->json($data, Response::HTTP_OK, $this->headers);
    }


    /**
     * @Route("/getContactsCustomFields", name="getContactsCustomFields",  methods={"GET"})
     * @return JsonResponse
     * @deprecated moved to ContactsController
     */
    public function getContactsCustomFields(Request $request): JsonResponse
    {
        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST, $this->headers);
        }
        $this->mioService->setApiKey($apikey);
        $data[ MioServices::CUSTOMFIELDS ] = $this->mioService->getContactsCustomFields($apikey);
        return $this->json($data, Response::HTTP_OK, $this->headers);
    }


    /**
     * @Route("/getMailingsDoiKeys", name="getMailingsDoiKeys",  methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     * @internal l slow ~4s
     */
    public function getMailingsDoiKeys(Request $request): JsonResponse
    {
        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST);
        }
        $this->mioService->setApiKey($apikey);
        $data[ MioServices::DOIKEYLIST ] = $this->mioService->getMailingsDoiKeys($apikey);
        return $this->json($data, Response::HTTP_OK, $this->headers);
    }


    /**
     * @Route("/getTransactionContactEvents", name="getTransactionContactEvents",  methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getTransactionContactEvents(Request $request): JsonResponse
    {
        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST);
        }
        $this->mioService->setApiKey($apikey);
        $data[ MioServices::CONTACTEVENTSLIST ] = $this->mioService->getTransactionContactEvents($apikey);
        return $this->json($data, Response::HTTP_OK, $this->headers);
    }


    /**
     * called by api-in-one::MSMioServices::integrateLeadToMIO
     * @Route( "/integratetomio", name="integratetomio",  methods={"POST"});
     * @param Request $request
     * @param ToolServices $toolServices
     * @param MioServices $mioServices
     * @param MioReturnType $mioReturnType
     * @return JsonResponse
     * @author jsr
     */
    public function integrateCampaignLeadToMio(
        Request $request,
        ToolServices $toolServices,
        MioServices $mioServices,
        MioReturnType $mioReturnType,
        LoggerInterface $logger
    ): JsonResponse {
        $content = $request->getContent();
        $logger->info('data '.json_encode($content));
        $contentData = $toolServices->unpackContent($content);
        $check = $toolServices->hasValidChecksum($contentData);
        if (!$check) {
            return $this->json(['status' => false, 'message' => 'content has invalid checksum'], Response::HTTP_OK, $this->headers);
        } else {
            $ret = $mioServices->runCreateNewContact($contentData);

            if ($ret[ $mioReturnType::SUCCESS ]) {
                return $this->json(['status' => true, 'message' => $ret[ $mioReturnType::MESSAGE ]], Response::HTTP_OK, $this->headers);
            } else {
                return $this->json(['status' => false, 'message' => $ret[ $mioReturnType::MESSAGE ]], Response::HTTP_OK, $this->headers);
            }
        }
    }

    /**
     * @Route("/can_i_post", name="can_i_post",  methods={"POST"})
     * @param MioServices $mioServices
     * @return JsonResponse
     */
    public function canIPost(Request $request, MioServices $mioServices)
    {
        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST, $this->headers);
        }

        if ($mioServices->mioPingCheckPost($apikey)) {
            return $this->json(['post' => true], Response::HTTP_OK, $this->headers);

        } else {
            return $this->json(['post' => false], Response::HTTP_OK, $this->headers);
        }
    }

    /**
     * @Route("/can_i_get", name="can_i_get",  methods={"POST"})
     * @param MioServices $mioServices
     * @return JsonResponse
     */
    public function canIGet(Request $request, MioServices $mioServices)
    {
        $apikey = $request->get('apikey');
        if (empty($apikey)) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST, $this->headers);
        }

        if ($mioServices->mioPingCheckGet($apikey)) {
            return $this->json(['get' => true], Response::HTTP_OK, $this->headers);
        } else {
            return $this->json(['get' => false], Response::HTTP_OK, $this->headers);
        }
    }


    /**
     * @Route("/create_custom_fields", name="create_customfield",  methods={"POST"})
     * @param Request $request
     * @param MioServices $mioServices
     * @return JsonResponse
     * @deprecated Moved to ContactsController
     */
    public function createCustomFields(
        Request $request,
        MioServices $mioServices,
        ToolServices $toolServices
    ): JsonResponse {
        $content = $request->get('text');
        #$content = $request->getContent();
        $contentData = $toolServices->unpackContent($content);

        if (empty($contentData[ 'api_key' ]) || !$mioServices->setApiKey($contentData[ 'api_key' ])) {
            return $this->json(['error' => 'no apikey'], Response::HTTP_BAD_REQUEST, $this->headers);
        }
        return $this->json($mioServices->createCustomFields($contentData[ 'custom_fields' ]), Response::HTTP_OK, $this->headers);
    }


    /**
     * @Route("/blacklist_email", name="blacklist_email",  methods={"POST"})
     * @param Request $request
     * @param MioServices $mioServices
     * @param ToolServices $toolServices
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @author Pradeep
     * @deprecated Create new Controller for Blacklisting
     */
    public function blacklistEmail(
        Request $request,
        MioServices $mioServices,
        ToolServices $toolServices,
        LoggerInterface $logger
    ): JsonResponse {
        $content = $request->getContent();
        $data = $toolServices->unpackContent($content);
        $blacklist_email = $data[ 'blk_email_list' ];
        $blacklist_id = $data[ 'blk_id' ];
        $api_key = $data[ 'api_key' ];
        $blacklist_import_name = $data[ 'blk_imp_name' ];
        if (empty($blacklist_email) || empty($api_key) || !$mioServices->setApiKey($api_key)) {
            $logger->info('no api_key or blacklist email', [__METHOD__, __LINE__]);
            return $this->json(['error' => 'no api_key or blacklist Emails'], Response::HTTP_BAD_REQUEST, $this->headers);
        }
        return $this->json($mioServices->blackListEmail($blacklist_id, $blacklist_email, $blacklist_import_name), Response::HTTP_OK, $this->headers);
    }
}
