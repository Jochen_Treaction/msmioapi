<?php

namespace App\Controller;

use App\Services\ToolServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\MioServices;
use Psr\Log\LoggerInterface;
use com_maileon_api_mailings_MailingsService as MailingsService;

class MailingsController extends AbstractController
{
    private $hasPermission = true;

    // TODO: move that to mailinone.yaml
    private $config = [
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => "",
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false",
    ];

	protected $headers;


	public function __construct(ToolServices $toolServices)
	{
		$this->headers = $toolServices->setHeadersToAvoidCORSBlocking();
	}


	/**
     * @Route("/get_doi_keys", name="get_doi_keys",  methods={"POST"})
     */
    public function getDoiKeys(Request $request, LoggerInterface $logger): JsonResponse
    {
        $doiMailings = [];
        $err = '';
        $apikey = $request->get('apikey');

        if (!empty($apikey)) {
            $this->config['API_KEY'] = $apikey;
        } else {
            return $this->json(['error' => 'no apikey'], Response::HTTP_OK, $this->headers);
        }

        // TODO: implement
        // $this->hasPermission = App\MioAccessService->validate($request->get('iam'));

        if ($this->hasPermission) {
            try {
                $mailingsService = new MailingsService($this->config);
                $mailingsService->setDebug(false);

                $response = $mailingsService->getMailingsByTypes(array('doi'));
                $res = $response->getResult();

                foreach ($res as $result) {
                    $doiKeys = $mailingsService->getDoiMailingKey((string)$result->id);
                    $mailingNames = $mailingsService->getName((string)$result->id);
                    $doiKey = \simplexml_load_string($doiKeys->getResult());
                    $doiMailing = array(
                        'doiKey' => (string)$doiKey[0],
                        'name' => (string)$mailingNames->getResult(),
                    );
                    array_push($doiMailings, $doiMailing);
                }

                // TODO: remove logging
                $logger->info('DOI Mailings Array =' . json_encode($doiMailings));
                return $this->json($doiMailings, Response::HTTP_OK, $this->headers);

            } catch (\com_maileon_api_MaileonAPIException $e) {
                $err = $e->getMessage();
            }

            if ($res->isSuccess()) {
                return $this->json($res->getResult(), Response::HTTP_OK,$this->headers);
            } else {
                return $this->json(['error' => $err], Response::HTTP_NO_CONTENT, $this->headers);
            }
        } else {
            return $this->json(['error' => 'not authorized'], Response::HTTP_UNAUTHORIZED, $this->headers);
        }
    }
}
