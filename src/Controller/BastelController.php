<?php

namespace App\Controller;

use App\Services\RestAPIServices\Blacklists\Blacklist;
use App\Services\RestAPIServices\Contacts\SynchronizeContacts;
use App\Services\RestAPIServices\Formatter\XMLSyncContactsListFormatter;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class BastelController extends AbstractController
{

    /**
     * @Route("/check/alive", name="checkAlive",  methods={"POST","GET"})
     */
    public function check()
    {
        return $this->json("I am alive");
    }
}