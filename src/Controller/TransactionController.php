<?php

namespace App\Controller;

use App\Services\ToolServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use com_maileon_api_transactions_TransactionsService as TransactionService;
use App\Services\MioServices;

class TransactionController extends AbstractController
{
    private $hasPermission = true;

    // TODO: move that to mailinone.yaml
    private $config = [
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => "",
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false"
    ];

    protected $headers;


    public function __construct(ToolServices $toolServices)
	{
		$this->headers = $toolServices->setHeadersToAvoidCORSBlocking();
	}


	/**
     * @Route("/get_contact_events", name="get_contact_events",  methods={"POST"})
     */
    public function contactEvents(Request $request, LoggerInterface $logger)
    {

        $logger->info('MY API-KEY = ' . $request->get('apikey'));

        $err = '';
        $apikey = $request->get('apikey');

        $logger->info('ser req = ' . $request->getRequestUri());

        if(!empty($apikey)) {
            $this->config['API_KEY'] = $apikey;
            $logger->info("the config is = ". json_encode($this->config));
        } else {
            return $this->json(['error' => 'no apikey'], Response::HTTP_OK, $this->headers);
        }

        // TODO: implement
        // $this->hasPermission = App\MioAccessService->validate($request->get('iam'));

        if ($this->hasPermission) {
            try {
                $transactionService = new TransactionService($this->config);
                $transactionService->setDebug(false);
                $res = $transactionService->getTransactionTypes(1,100);
                $logger->info(json_encode($res));


            } catch (\com_maileon_api_MaileonAPIException $e) {
                $err = $e->getMessage();
            }

            if ($res->isSuccess()) {
                return $this->json($res->getResult(), Response::HTTP_OK, $this->headers);
            } else {
                return $this->json(['error' => $err], Response::HTTP_NO_CONTENT, $this->headers);
            }
        } else {
            return $this->json(['error' => 'not authorized'], Response::HTTP_UNAUTHORIZED, $this->headers);
        }
    }


	/**
	 * returns all Contact Events along with assigned attributes -> used by MIO processhook configuration
	 * @Route("/transaction/types", name="transaction.types",  methods={"GET"})
	 * @param Request     $request
	 * @param MioServices $mioServices
	 * @return JsonResponse
	 */
	public function getTransactionTypes(Request $request, MioServices $mioServices):JsonResponse
	{
		$apikey = $request->get('apikey');
		$transactionTypes = $mioServices->getTransactionTypes($apikey);

		if(!empty($transactionTypes)) {
			return $this->json($transactionTypes, Response::HTTP_OK, $this->headers);
		}

		return $this->json([], Response::HTTP_OK, $this->headers);
    }
}
