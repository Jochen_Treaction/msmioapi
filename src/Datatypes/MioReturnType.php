<?php


namespace App\Datatypes;

use Psr\Log\LoggerInterface;

class MioReturnType
{
    public $success;
    public $statusCode;
    public $message;

    const SUCCESS='success';
    const STATUSCODE='statusCode';
    const MESSAGE='message';

    protected $logger;

    /**
     * MioReturnType constructor.
     * @param LoggerInterface $logger
     * @author jsr
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->success = '';
        $this->statusCode = '';
        $this->message = '';
    }

    /**
     * @param bool $success
     * @param string $statusCode
     * @param string $message
     * @return MioReturnType
     * @author jsr
     */
    public function set(bool $success, string $statusCode, string $message): MioReturnType
    {
        $this->success = htmlentities(filter_var($success, FILTER_VALIDATE_BOOLEAN));
        $this->statusCode = htmlentities(filter_var($statusCode, FILTER_SANITIZE_STRING));
        $this->message = htmlentities(filter_var($message, FILTER_SANITIZE_STRING));
        return $this;
    }


    /**
     * @return array
     * @author jsr
     */
    public function toArray(): array
    {
        return [
            self::SUCCESS => $this->success,
            self::STATUSCODE => $this->statusCode,
            self::MESSAGE => $this->message,
        ];
    }


    /**
     * @param array $array
     * @return MioReturnType
     */
    public function arrayToMioReturnType(array $array): MioReturnType
    {
        $this->success = (isset($array[self::SUCCESS])) ? $array[self::SUCCESS] : '';
        $this->statusCode = (isset($array[self::STATUSCODE])) ? $array[self::STATUSCODE] : '';
        $this->message = (isset($array[self::MESSAGE])) ? $array[self::MESSAGE] : '';
        return $this;
    }


    /**
     * @return MioReturnType
     * @author jsr
     */
    public function get(): MioReturnType
    {
        return $this;
    }

    /**
     * @return string
     * @author jsr
     */
    public function getJson(): string
    {
        return $this->__toString();
    }


    /**
     * @return string
     * @author jsr
     */
    public function __toString(): string
    {
        try {
            $ret = (string)json_encode([
                self::MESSAGE => $this->success,
                self::STATUSCODE => $this->statusCode,
                self::MESSAGE => $this->message,
            ],
                JSON_THROW_ON_ERROR);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return '';
        }
        return $ret;
    }
}
