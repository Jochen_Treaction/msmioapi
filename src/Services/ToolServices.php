<?php


namespace App\Services;

use JsonException;
use Psr\Log\LoggerInterface;
use Exception;

class ToolServices
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param array $twoDimArray
     * @return bool
     * @author jsr
     */
    public function hasValidChecksum(array $twoDimArray): bool
    {
        $twoDim = $twoDimArray;
        unset($twoDim[ 'checksum' ]);
        return ($twoDimArray[ 'checksum' ] === $this->getChecksum($twoDim));
    }

    /**
     * @param array $twoDimArray
     * @return string|null
     * @author jsr
     */
    protected function getChecksum(array $twoDimArray): ?string
    {
        try {
            $hash = hash('sha512', str_rot13(json_encode($twoDimArray, JSON_THROW_ON_ERROR)));
            return $hash;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
    }

    /**
     * @param string $content = base64_encode(json_encode($array))
     * @return mixed|null
     * @author jsr
     */
/*    public function unpackContent(string $content, bool $useGZCompress = false)
    {
        if (empty($content)) {
            return null;
        }

        try {
            $unpackedContent = json_decode(base64_decode($content), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return null;
        }
        return $unpackedContent;
    }*/

	/**
	 * @param Response $response
	 * @return Response
	 * @author Pradeep
	 */
	public function setHeadersToAvoidCORSBlocking(): array
	{
		return [
			'Content-Type' => 'application/json',
			'Access-Control-Allow-Origin' => '*',
			'Access-Control-Allow-Headers' => 'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer',
			'Access-Control-Max-Age' => '86400',
			'Access-Control-Allow-Methods' => 'PUT, GET, POST, DELETE',
		];
	}

    /**
     * decryptContentData decrypts the payload
     * @param string $encodedString
     * @param bool $useGZCompress
     * @return array
     * @author Pradeep
     */
    public function unpackContent(string $encodedString, bool $useGZCompress = false): array
    {
        $data = [];
        try {
            if (empty($encodedString)) {
                return $data;
            }
            $decodedString = base64_decode($encodedString);
            // decompress the data if 'gzcompress' is used to compress the data.
            if ($useGZCompress) {
                $decodedString = gzuncompress($decodedString);
            }
            $array = json_decode($decodedString, true, 512, JSON_THROW_ON_ERROR | JSON_INVALID_UTF8_IGNORE);
            if (!empty($array)) {
                $data = $array;
            }
            return $data;
        } catch (JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $data;
        }
    }
}
