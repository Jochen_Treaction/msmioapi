<?php

namespace App\Services\WebProtocolServices;

use JsonException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HttpClientService implements WebProtocolInterface
{
    /**
     * @var HttpClientInterface
     * @author Pradeep
     */
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient, LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
    }

    /**
     * @param string $method
     * @param string $payload
     * @param string $endpoint
     * @param array $headers
     * @return int
     * @author Pradeep
     */
    public function send(string $method, string $payload, string $endpoint, array $headers = [])
    {
        $this->logger->info("send :".json_encode(['payload' => $payload, 'endpoint' => $endpoint, 'headers' => $headers]));
        try {
            $response = $this->httpClient->request($method, $endpoint, ['headers' => $headers, 'body' => $payload]);
            if ($response === null) {
                $this->logger->info("empty response");
            }
            $this->logger->info("not empty response " . json_encode($response, JSON_THROW_ON_ERROR));
           // $info = $response->getInfo();
            $content = $response->getContent();
           // $toArray = $response->toArray();
            $code = $response->getStatusCode();
           // $this->logger->info('info '.json_encode($info));
            $this->logger->info('Status Code ' . json_encode($code));
            $this->logger->info('Content Info ' . json_encode($content));
            //$this->logger->info('Array Info ' . json_encode($toArray));
            return $response->getStatusCode();
        } catch (ClientExceptionInterface | JsonException |
            DecodingExceptionInterface | RedirectionExceptionInterface |
            ServerExceptionInterface | TransportExceptionInterface $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return Response::HTTP_INTERNAL_SERVER_ERROR;
        }
    }
}