<?php

namespace App\Services\WebProtocolServices;

interface WebProtocolInterface
{

    public function send(string $method, string $payload, string $endpoint, array $properties=[]);

}