<?php


namespace App\Services;

use com_maileon_api_MaileonAPIException;
use Exception;
use Psr\Log\LoggerInterface;
use App\Datatypes\MioReturnType;
use com_maileon_api_transactions_TransactionsService as TransactionsService;
use com_maileon_api_transactions_Transaction as Transaction;
use com_maileon_api_transactions_ContactReference as ContactReference;
use com_maileon_api_contacts_ContactsService as ContactsService;
use com_maileon_api_contacts_Contact as MioContact;
use com_maileon_api_mailings_MailingsService as MailingsService;
use com_maileon_api_utils_PingService as PingService;
use com_maileon_api_contacts_StandardContactField as StandardContactFieldsXY;
use com_maileon_api_reports_ReportsService as ReportsService;
use com_maileon_api_contacts_Permission as Permission;
use com_maileon_api_contacts_SynchronizationMode as SynchronizationMode;
use com_maileon_api_marketingautomation_MarketingAutomationService as MarketingAutomationService;
use com_maileon_api_blacklists_BlacklistsService as BlacklistsService;
use RuntimeException;
use function simplexml_load_string;

class MioServices
{
    const INSTRUCTIONS = 'instructions';
    const INTEGRATIONTYPESMIO = 'mioIntegrationTypes';
    const STANDARDFIELDS = 'standardFields';
    const CUSTOMFIELDS = 'customFields';
    const CONTACTEVENTFIELDS = 'contactEventFields';
    const DOIKEYLIST = 'doiKeyList';
    const CONTACTEVENTSLIST = 'contactEventsList';

    const INST_UNSUBSCRIBER = 'unsubscriberStatus';
    const INST_APIKEY = 'apikey';
    const INST_CAMPAIGN_NAME = 'campaignName';
    const INST_DOI_PERMISSION = 'doiPermission';
    const INST_DOIMAILING_KEY = 'doiMailingKey';
    const INST_CONTACT_EVENT_ID = 'contactEventId';
    const INST_MARKETING_AUTOMATION_ID = 'marketingAutomationId';
    const INST_INTEGRATION_TYPE = 'typeOfIntegration';

    // const ITYPE_NONE = 'None';
    const ITYPE_MA = 'MarketingAutomation';
    const ITYPE_DOI = 'DOI';
    const ITYPE_CE = 'ContactEvent';
    const ITYPE_DOI_XOR_CE = 'DOIXorContactEvent';

    const EMAIL = 'EMAIL';

    // DataTypes
    const DATATYPE_STRING = 'string';
    const DATATYPE_INTEGER = 'integer';
    const DATATYPE_DATE = 'date';
    const DATATYPE_FLOAT = 'float';
    const DATATYPE_BOOLEAN = 'boolean';

    protected $logger;
    protected $ret;
    protected $apiKey;


    protected $config = [
        "BASE_URI" => "https://api.maileon.com/1.0",
        "API_KEY" => "",
        "PROXY_HOST" => "",
        "PROXY_PORT" => "",
        "THROW_EXCEPTION" => true,
        "TIMEOUT" => 300,
        "DEBUG" => "false",
    ];


    public function __construct(LoggerInterface $logger, MioReturnType $mioReturnType)
    {
        $this->logger = $logger;
        $this->ret = $mioReturnType;
        $this->apiKey = '';
    }


    /**
     * @param array $customField
     * @param bool $appendMIOTag
     * @return bool
     * @author Pradeep
     */
    public function createCustomFields(array $customField, bool $appendMIOTag = false)
    {
        $status = false;
        if (empty($customField)) {
            return $status;
        }
        $contactService = new ContactsService($this->config);
        $contact_custom_fields = $this->getContactsCustomFields();

        if (empty($customField[ 'name' ])) {
            $this->logger->warning('Empty CustomFieldName', [__METHOD__, __LINE__]);
            return $status;
        }
        // Validates Field type.
        $datatype = ($this->validateDataType($customField[ 'datatype' ])) ? $customField[ 'datatype' ] : self::DATATYPE_STRING;
        $sanitized_string = $this->sanitizeString($customField[ 'name' ]);
        // append MIO tag if parameter is set.
        if ($appendMIOTag) {
            $sanitized_string = 'MIO_' . $sanitized_string;
        }
        // Check if the custom field is already present in eMIO account.
        // Create custom Field only if the field is not existed.
        // Updating the dataType is currently not supported.
        if (!isset($contact_custom_fields[ $sanitized_string ])) {
            $status = $contactService->createCustomField($sanitized_string, $datatype);
        } else {
            $status = true;
        }

        return $status;
    }

    private function sanitizeString(string $string):string
    {
        // Sanitize string
        $sanitized_string = filter_var($string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        // Replace German Characters
        $sanitized_string = preg_replace(['/Ä/', '/ä/', '/Ö/', '/ö/', '/ß/', '/Ü/', '/ü/', '/\s+/'],
            ['AE', 'ae', 'OE', 'oe', 'ss', 'UE', 'ue', '_'], $sanitized_string);
        // Replace '&' character
        return str_replace(array(
            '&',
            '/',
            '!',
            '"',
            '§',
            '$',
            '%',
            '#',
            '*',
            '+',
            ';',
            ',',
            '>',
            '<',
            '|',
        ),
            array('-', '-', '', '', '', '', '', '', '', '', '', '', '', '', ''), $sanitized_string);
    }

    /**
     * @param string $apiKey
     * @return array
     * @author jsr
     */
    public function getContactsCustomFields(string $apiKey = null): array
    {
        // early exit
        if (empty($apiKey) && empty($this->apiKey)) {
            return [];
        } elseif (!empty($this->apiKey)) {
            $apiKey = $this->apiKey;
        }

        $this->config[ 'API_KEY' ] = $apiKey;
        $return_array = [];
        $t1 = microtime(true);
        try {
            $contactsService = new ContactsService($this->config);
            $contactsService->setDebug(false);
            $res = $contactsService->getCustomFields();

            if ($res->isSuccess()) {
                $records = json_decode(json_encode($res->getResult(), JSON_FORCE_OBJECT, 512), true, 512,
                    JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);

                if (!empty($records)) {
                    $records = json_decode(json_encode($res->getResult(), JSON_FORCE_OBJECT, 512), true, 512,
                        JSON_THROW_ON_ERROR | JSON_OBJECT_AS_ARRAY);

                    foreach ($records[ 'custom_fields' ] as $k => $r) {
                        $return_array[ $k ] = $r[ 0 ];
                    }
                }
            } else {
                return ['error' => 'request failed'];
            }
        } catch (com_maileon_api_MaileonAPIException $e) {
            $err = $e->getMessage();
            $this->logger->error($err, [__METHOD__, __LINE__]);
            return ['error' => 'request failed'];
        }
        $t2 = microtime(true);
        $d = ($t2 - $t1);
        $this->logger->error("mio duration(getCustomFields) $d seconds", [__METHOD__, __LINE__]);

        return $return_array;
    }

    /**
     * @param $dataType
     * @return bool
     * @author Pradeep
     * @internal  Validates whether given DataType is supported by eMIO or not.
     */
    private function validateDataType($dataType): bool
    {
        if (empty($dataType)) {
            return false;
        }
        if (!in_array($dataType, [
            self::DATATYPE_BOOLEAN,
            self::DATATYPE_DATE,
            self::DATATYPE_FLOAT,
            self::DATATYPE_INTEGER,
            self::DATATYPE_STRING,
        ], true)) {
            return false;
        }
        return true;
    }

    /**
     * @param string $blacklist_email
     * @param int $blacklist_id
     * @param string|null $blacklist_import_name
     * @return bool
     * @author Pradeep
     */
    public function blackListEmail(
        int $blacklist_id,
        string $blacklist_email,
        string $blacklist_import_name = null
    ): bool {
        $status = false;
        if (empty($blacklist_email) || $blacklist_id <= 0) {
            $this->logger->error('Invalid Blacklist email or id', [__METHOD__, __LINE__]);
            return $status;
        }
        $blacklist_service = new BlacklistsService($this->config);
        $blacklist_service->setDebug(false);
        if ($blacklist_service->getBlacklist($blacklist_id) === null) {
            $this->logger->error('Invalid blacklist id configured', [__METHOD__, __LINE__]);
            return $status;
        }
        $blk_email_arr = explode(',', $blacklist_email);
        $blk_resp = $blacklist_service->addEntriesToBlacklist($blacklist_id, $blk_email_arr,
            $blacklist_import_name);
        if (!$blk_resp->isSuccess() || $blk_resp->isClientError()) {
            $this->logger->error('Error while updating Blacklist service ', [__METHOD__, __LINE__]);
            return $status;
        }

        return true;
    }

    /**
     * @return array
     * @author Pradeep
     * @internal Get the list of blacklists and parses to key value pair based on id and name .
     */
    public function getBlacklist(): array
    {
        $blacklists = [];
        if (empty($this->config) || !isset($this->config[ 'API_KEY' ])) {
            $this->logger->error('Invalid eMIO Config or APIKey provided');
            return $blacklists;
        }
        $blacklistService = new BlacklistsService($this->config);
        $blacklistService->setDebug(false);
        $response = $blacklistService->getBlacklists();
        $resultInXML = $response->getResultXML();
        foreach ($resultInXML as $result) {
            // ELEMENAT EMPTY BLACKLISTING ID'S
            if (!isset($result->id) || empty($result->id) || empty($result->name)) {
                continue;
            }
            $id = trim($result->id);
            $name = trim($result->name);
            $blacklists[] = [
                'id' => $id,
                'name'=> $name
            ];
        }
        return $blacklists;
    }

    /**
     * @return array
     * @author jsr
     */
    public function getContactEventFields(): array
    {
        return [];
    }

    /**
     * @return array
     * @author jsr
     */
    public function getContactsStandardFields(): array
    {
        // Todo added comment and datatype field , Need to test further execution of this function
        return [
            self::EMAIL => [
                'dataType' => 'string',
                'comment' => 'A valid email address. This field is not a MIO standard field, but the key of each contact!',
            ],
            StandardContactFieldsXY::$SALUTATION => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$GENDER => [
                'dataType' => 'string',
                'comment' => 'Allowed values: m (male) and f (female)',
            ],
            StandardContactFieldsXY::$TITLE => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$FIRSTNAME => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$LASTNAME => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$FULLNAME => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],

            StandardContactFieldsXY::$ORGANIZATION => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$ADDRESS => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$HNR => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$ZIP => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$CITY => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$COUNTRY => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$REGION => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],
            StandardContactFieldsXY::$STATE => [
                'dataType' => 'string',
                'comment' => 'The length must not exceed 255 characters.',
            ],

            StandardContactFieldsXY::$BIRTHDAY => [
                'dataType' => 'date',
                'comment' => 'Allowed patterns: yyyy-MM-dd and yyyy-MM-dd HH:mm:ss',
            ],
            StandardContactFieldsXY::$NAMEDAY => [
                'dataType' => 'date',
                'comment' => 'Allowed patterns: yyyy-mm-dd and yyyy-mm-dd hh:mm:ss',
            ],

            StandardContactFieldsXY::$LOCALE => [
                'dataType' => 'string',
                'comment' => 'The value must follow the pattern: xx (example: en) or xx_XX (example: de_DE). „xx" is the two character language code (ISO 639) and „XX" the two character code for the country of the language (ISO 3166). Example: british english is notated as „en_GB" and american english as „en_US". For just marking a contact as english speaking „en" is usually enough.',
            ],
        ];
    }

    /**
     * @return array
     * @author jsr
     */
    public function getInstructionFields(): array
    {
        return [
            self::INST_APIKEY => 'Maileon Api Key',
            self::INST_DOI_PERMISSION => 'value of MIO DOI permission (1,2,3,4,5)',
            self::INST_DOIMAILING_KEY => 'the DOI key, which shall be triggered in MIO',
            self::INST_CAMPAIGN_NAME => 'name of the campaign where the contact comes from',
            self::INST_CONTACT_EVENT_ID => 'a contact event id of MIO',
            self::INST_MARKETING_AUTOMATION_ID => 'a markting automation id of MIO',
            self::INST_INTEGRATION_TYPE => 'type of integration field. DOI, MA, CE, DOI xor CE, ...',
            // values see getIntegrationTypes()
        ];
    }

    /**
     * @return array
     * @author jsr
     */
    public function getIntegrationTypes()
    {
        return [
            // self::ITYPE_NONE => 'None',
            self::ITYPE_MA => 'Marketing Automation',
            self::ITYPE_DOI => 'Double Opt In',
            self::ITYPE_CE => 'Contact Event',
            self::ITYPE_DOI_XOR_CE => 'Either DOI or Contact Event',
        ];
    }

    /**
     * @param string $apiKey
     * @return array
     * @internal return both marketing automation and contact events
     * @author   jsr
     */
    public function getTransactionContactEvents(string $apiKey): array
    {
        if (empty($apiKey)) {
            return [];
        } // early exit
        $this->config[ 'API_KEY' ] = $apiKey;

        try {
            $transactionService = new TransactionsService($this->config);
            $transactionService->setDebug(false);
            $res = $transactionService->getTransactionTypes(1, 100);
            // TODO: remove
            $this->logger->error('RES=', [$res], [__METHOD__, __LINE__]);
        } catch (com_maileon_api_MaileonAPIException $e) {
            $err = $e->getMessage();
            $this->logger->error('ERROR', [$err], [__METHOD__, __LINE__]);
        }

        if ($res->isSuccess()) {
            return $res->getResult();
        } else {
            return [];
        }
    }

    private function validateEmail(string $eMail)
    {
        if(empty($eMail) || !filter_var($eMail,FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        return true;
    }
    /**
     * @param array $contactData
     * $contactData['instruction'][<keys>]      <keys> = any instruction like e.g. $contactData['instruction']['setDoi'] = true
     * $contactData['standardfields'][<keys>]   <keys> = StandardContactField
     * $contactData['customfields'][<keys>]     <keys> = CustomField, allowed characters [a-zA-Z0-9]{3,}
     * @return array ['success' => true|false, 'status' => http-statuscode of MIO response]
     * @author jsr
     */
    public function runCreateNewContact(array $contactData): array
    {
        $startTime = microtime(true);
        $this->logger->error('PARAM IN \$CONTACTDATA = ' . json_encode($contactData), [__METHOD__, __LINE__]);

        $eMail = $contactData[ self::STANDARDFIELDS ][ self::EMAIL ] ?? '';
        $instructions  = $contactData[ self::INSTRUCTIONS ] ?? '';
        $instructionType = $contactData[ self::INSTRUCTIONS ][ self::INST_INTEGRATION_TYPE ] ?? '';
        $apikey = $contactData[ self::INSTRUCTIONS ][ self::INST_APIKEY ] ?? '';
        $doiKey  =$contactData[ self::INSTRUCTIONS ][ self::INST_DOIMAILING_KEY ] ??'';
        $doiPermission = $contactData[ self::INSTRUCTIONS ][ self::INST_DOI_PERMISSION ] ?? '';
        $unsubscriber  = $contactData[ self::INSTRUCTIONS ][ self::INST_UNSUBSCRIBER ] ??'';
        $campaignName = $contactData[ self::INSTRUCTIONS ][ self::INST_CAMPAIGN_NAME ] ?? '';

        // Validating for Email and StandardFields
        if (empty($apikey) || !$this->validateEmail($eMail)) {
            return $this->ret->set(false, '', ' missing/wrong email or missing apikey')->toArray();
        }

        // set APIKey
        $this->setApiKey($apikey);

        // Check if Email exists and get Contact Information
        $contactInMio = $this->getContactByEmail($eMail, $apikey);
        $this->logger->info('Contact IN MIO ' . json_encode($contactInMio, JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        // Contact exists in eMIO
        $contactExists = (!empty($contactInMio));
        $this->logger->info('$contactExists ' . json_encode($contactExists, JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        // Contact exists in eMIO with Some permission with active i,e with permission 1,2,3
        $hasDoi = $contactExists && $this->hasDoi($contactInMio[ 'permission' ]); // permission <> 2, 3, 4, 5
        $this->logger->info('$hasDoi ' . json_encode($hasDoi, JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        // Contact not exists in eMIO but configured to fire DOIKey now.
        $canFireDoi = (!$hasDoi && !empty($doiKey));
        $this->logger->info('$canFireDoi ' . json_encode($canFireDoi, JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        if (!$this->checkUnsubscriberConfig($eMail, (bool)$unsubscriber)) {
            return $this->ret->set(false, '', 'Contact is Unsubscribed')->toArray();
        }

        $validationResult = $this->validateInstructions($instructions);
        $this->logger->info('$validationResult ' . json_encode($validationResult, JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        if ($validationResult[ 'error' ]) {
            return $this->ret->set(false, '',
                'Wrong settings in provided data => check block "instructions": ' . $validationResult[ 'message' ])->toArray();
        }

        $runIntegration = in_array($validationResult[ 'runintegration' ],
            [self::ITYPE_MA, self::ITYPE_CE, self::ITYPE_DOI, self::ITYPE_DOI_XOR_CE], true);

        $this->logger->info('$runIntegration ' . json_encode($runIntegration), [__METHOD__, __LINE__]);

        if ($runIntegration) {
            $this->logger->error('RUNNING INTEGRATION ...', [__METHOD__, __LINE__]);

            //init
            $doi = false;
            $doiPlus = false;
            $doiMailingKey = '';

            $contactsService = new ContactsService($this->config);
            $contactsService->setDebug(false);
            $newContact = new MioContact();
            $this->setStandardFields($newContact, $contactData); // assign standard fields
            $this->setCustomFields($newContact, $contactData); // assign custom fields

            $newContact->id = ($contactExists && !empty($contactInMio[ 'id' ])) ? $contactInMio[ 'id' ] : null;
            $newContact->anonymous = false;

            $this->logger->error('INTEGRATION TYPE = ' . $instructionType, [__METHOD__, __LINE__]);
            if ($instructionType === self::ITYPE_MA) {
                $newContact->permission = Permission::$DOI_PLUS; // as in plenty Plugin
            } else {
                $permission = ($contactExists && ((int)$doiPermission <= (int)$contactInMio[ 'permission' ])) ?
                    $contactInMio[ 'permission' ] : $doiPermission;
                $newContact->permission = $this->getDoiPermission($permission);
            }

            $newContact->email = strtolower($eMail);
/*            $tempCampaignName = $this->cleanString($campaignName);
            $campaignNameOfContact = (!empty($tempCampaignName)) ? $tempCampaignName : '';
            $this->logger->info('$campaignNameOfContact ' . json_encode($campaignNameOfContact, JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);*/
            $middleTime = $startTime - microtime(true);
            $this->logger->info('MiddleTime: '.json_encode($middleTime));
            switch ($validationResult[ 'runintegration' ]) {
                case self::ITYPE_DOI_XOR_CE:
                    if ($contactExists && $hasDoi) {
                        return $this->runItypeCE($newContact, $contactData, $contactExists);
                    }
                    return $this->runItypeDOI($newContact, $contactData,  $contactExists, $hasDoi, $canFireDoi);
                    break;
                case self::ITYPE_DOI:
                    return $this->runItypeDOI($newContact, $contactData, $contactExists, $hasDoi, $canFireDoi);
                    break;
                case self::ITYPE_CE:
                    return $this->runItypeCE($newContact, $contactData, $contactExists);
                    break;
                case self::ITYPE_MA:
                    return $this->runItypeMA($newContact, $contactData, $contactExists);
                    break;
                default:

                    $this->logger->error('IN DEFAULT', [__METHOD__, __LINE__]);
                    return $this->ret->set(false, '', 'no integration configured')->toArray();
                    break;
            }
        } else {
            $this->logger->error('EXIT: missing integration instruction', [__METHOD__, __LINE__]);
            return $this->ret->set(false, '', 'missing integration instruction')->toArray();
        }
    }

    /**
     * @param $newContact
     * @param $contactData
     * @param $contactExistsInMIO
     * @return array
     * @author Pradeep
     */
    private function runItypeMA($newContact, $contactData, $contactExistsInMIO)
    {
        $this->logger->error('IN ' . self::ITYPE_MA, [__METHOD__, __LINE__]);
        $marketingAutomationId = $contactData[ self::INSTRUCTIONS ][ self::INST_MARKETING_AUTOMATION_ID ] ?? 0;
        $campaignNameOfContact = $contactData[ self::INSTRUCTIONS ][ self::INST_CAMPAIGN_NAME ] ?? '';

        if (!empty($marketingAutomationId)) {
            // insert or unsetEmailList contact.
            $retCreateContact = $this->insertOrUpdateContact($newContact, $campaignNameOfContact, false,
                false, '');

            if (false === $retCreateContact[ MioReturnType::SUCCESS ] && !$contactExistsInMIO) {
                return $this->ret->set(false, $retCreateContact[ MioReturnType::STATUSCODE ],
                    'Can not run MA. Contact does not exist aand could not be created in MIO: ' . $retCreateContact[ MioReturnType::MESSAGE ])->toArray();
            } else {
                sleep(2); // give some time to MIO to persist submitted data
            }
            // run Marketing Automation
            return $this->runMA($newContact->email, (int)$marketingAutomationId);
        } else {
            return $this->ret->set(false, '',
                'no marketingAutomationId provided for ' . self::ITYPE_MA)->toArray();
        }
    }

    /**
     * @param $newContact
     * @param $contactData
     * @param $contactExists
     * @return array
     * @author Pradeep
     */
    private function runItypeCE($newContact, $contactData, $contactExists)
    {
        $this->logger->error('IN ' . self::ITYPE_CE, [__METHOD__, __LINE__]);
        $campaignNameOfContact = $contactData[ self::INSTRUCTIONS ][ self::INST_CAMPAIGN_NAME ] ?? '';
        $contactEventId = $contactData[ self::INSTRUCTIONS ][ self::INST_CONTACT_EVENT_ID ] ?? 0;
        $contactEventFields  = !empty($contactData[ self::CONTACTEVENTFIELDS ]) ? $contactData[ self::CONTACTEVENTFIELDS ] : [];

        if (!$contactExists) {
            $doi = true;
            $doiPlus = true;
            $doiMailingKey = '';
            $this->insertOrUpdateContact($newContact, $campaignNameOfContact, $doi, $doiPlus,
                $doiMailingKey);
        }

        if (!empty($contactEventId) && !empty($contactEventFields)) {
            return $this->runCE($contactEventFields, $newContact->email, (int)$contactEventId);
        }

        return $this->ret->set(false, '',
            'no contactEventId or empty field-list provided for ' . self::ITYPE_CE)->toArray();
    }

    /**
     * @param $newContact
     * @param $contactData
     * @param $contactExists
     * @param $hasDoi
     * @param $canFireDoi
     * @return array
     * @throws \JsonException
     * @author Pradeep
     */
    private function runItypeDOI($newContact, $contactData, $contactExists, $hasDoi, $canFireDoi)
    {
        $startTime = microtime(true);
        $this->logger->info('ITYPE_DOI ' . json_encode($newContact), [__METHOD__, __LINE__]);

        $campaignNameOfContact = $contactData[ self::INSTRUCTIONS ][ self::INST_CAMPAIGN_NAME ] ?? '';
        $doiKey  =$contactData[ self::INSTRUCTIONS ][ self::INST_DOIMAILING_KEY ] ??'';

        // todo : 'isValidDoiKey' is taking around 6.9 seconds.
        // optimization or need to change to rest api.

        $isValidDoiKey = true; //$this->isValidDoiKey($doiKey);
        $this->logger->info('If Condition ' . json_encode([
                'isValidDoiKey' => $isValidDoiKey,
                '$canFireDoi' => $canFireDoi
            ], JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        if ($canFireDoi && $isValidDoiKey) {
            $doi = true;
            $doiPlus = true;
            $shouldFireDoiMail = !(($newContact->permission->code === 4 || $newContact->permission->code === 5));
            $doiMailingKey = $shouldFireDoiMail ? $doiKey : '';
            $this->logger->info('Inside If Condition ' . json_encode([
                    '$doi' => $doi,
                    '$doiPlus' => $doiPlus,
                    '$shouldFireDoiMail' => $shouldFireDoiMail,
                    '$doiMailingKey' => $doiMailingKey
                ], JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);
            //$doiMailingKey = $this->fetchDoiKey($contactData[self::INSTRUCTIONS][self::INST_DOIMAILING_KEY]);
        } else {
            // Default doi mailling will not send if the $doi is false.
            $doi = true;
            //$doi = $contactExists && $hasDoi;
            $doiPlus = true ;//$contactExists && $hasDoi;
            $doiMailingKey = '';
            $this->logger->info('Inside else Condition ' . json_encode([
                    '$doi' => $doi,
                    '$doiPlus' => $doiPlus,
                    '$doiMailingKey' => $doiMailingKey
                ], JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);
        }
        $this->logger->info('Before insertOrUpdateContact ' . json_encode([
                '$newContact' => $newContact,
                '$campaignNameOfContact' => $campaignNameOfContact,
                '$doi' => $doi,
                '$doiPlus' => $doiPlus,
                '$doiMailingKey' => $doiMailingKey
            ], JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);

        $diff = $startTime - microtime(true);
        $this->logger->info('runItypeDOI '. json_encode($diff));
        return $this->insertOrUpdateContact($newContact, $campaignNameOfContact, $doi, $doiPlus, $doiMailingKey);
    }

    /**
     * @param string $apiKey
     * @return bool
     * @author jsr
     */
    public function setApiKey(string $apiKey)
    {
        $this->apiKey = $apiKey;
        if (empty($apiKey)) {
            return false;
        } // early exit
        $this->config[ 'API_KEY' ] = $apiKey;
        return true;
    }

    /**
     * @param string $email
     * @param string|null $apiKey
     * @return array
     * @author jsr
     */
    public function getContactByEmail(string $email, string $apiKey = null): array
    {
        $this->logger->error('IN getContactByEmail ', [__METHOD__, __LINE__]);
        // check apikey
        if (empty($apiKey)) {
            if ($this->isApiKeySet()) {
                $this->logger->error('NO APIKEY', [__METHOD__, __LINE__]);// check apikey
                // okay
            } elseif (null !== $apiKey) {
                $this->logger->error('SET APIKEY', [__METHOD__, __LINE__]);
                $this->setApiKey($apiKey);
            } else {
                $this->logger->warning('Missing MIO ApiKey', [__METHOD__, __LINE__]);
                return [];
            }
        }

        // check email
        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->logger->error('EXIT: EMAIL IS NOT EMAIL ' . $email, [__METHOD__, __LINE__]);
            return [];
        }


        $this->logger->error('CONFIG ON CALL OF ContactsService ' . json_encode($this->config), [__METHOD__, __LINE__]);
        $this->logger->error("CONFIG ON CALL OF ContactsService with email =|{$email}|", [__METHOD__, __LINE__]);
        try {
            $contactService = new ContactsService($this->config);
            $contactService->setDebug(false);
            $response = $contactService->getContactByEmail($email);
        } catch (Exception $e) {
            $this->logger->error('EXIT DUE TO ' . $e->getMessage(), [__METHOD__, __LINE__]);
            return [];
        }
        if ($response->isSuccess()) {
            $this->logger->error('SUCCESSFUL RESPONSE', [__METHOD__, __LINE__]);
            $contact = $response->getResult();

            $this->logger->error(json_encode([
                'id' => $contact->id,
                'email' => $contact->email,
                'permission' => $contact->permission->code,
                'permission_type' => $contact->permission->type,
            ], JSON_THROW_ON_ERROR), [__METHOD__, __LINE__]);
            return [
                'id' => $contact->id,
                'email' => $contact->email,
                'permission' => $contact->permission->code,
                'permission_type' => $contact->permission->type,
            ];
        } else {
            return [];
        }
    }

    /**
     * @return bool
     * @author jsr
     */
    protected function isApiKeySet(): bool
    {
        return (!empty($this->config[ 'API_KEY' ]));
    }

    /**
     * @param int|Permission $permission
     * @return bool
     * @author jsr
     */
    protected function hasDoi($permission): bool
    {
        if ($permission instanceof Permission) {
            $perm = $permission->getCode();
        } elseif (is_int($permission)) {
            $perm = $permission;
        }
        return in_array($perm, [4, 5]);
    }

    /**
     * @param string $email
     * @param bool $unsubscriber_status
     *
     * // dont create a new contact - false
     * // is unsubscribed and $unsubscriber_status is false.
     * // create a new contact.    - true
     * // is not unsubscribed.
     * // contact not present.
     * // is unsubscribed and $unsubscriber_status is true.
     *
     * @return bool
     * @author Pradeep
     */
    private function checkUnsubscriberConfig(string $email, bool $unsubscriber_status): bool
    {
        $status = false;

        // continue to create contact
        $is_email_unsubscribed = $this->isUnsubscribed($email);

        // email not unsubscribed OR Unsubscriber status is true.
        if (!$is_email_unsubscribed || $unsubscriber_status) {
            $status = true;
        }

        return $status;
    }

    /**
     * @param string $email
     * @return bool
     */
    public function isUnsubscribed(string $email)
    {
        $reportsService = new ReportsService($this->config);
        $reportsService->setDebug(false);
        $response = $reportsService->getUnsubscribersCount(null, null, null, null, [$email]);

        return ((int)$response->getResult()) > 0; // not unsubsribed => still subscribed
    }

    protected function validateInstructions(array $instructions): array
    {
        $hasErrors = true;
        $message = '';
        $runIntegration = '';

        if (empty($instructions[ self::INST_APIKEY ])) {
            $message = 'you must provide a Mail-in-One ApiKey';
            return [
                'error' => $hasErrors,
                'message' => $message,
                'runintegration' => $runIntegration,
            ];
        }

        switch ($instructions[ self::INST_INTEGRATION_TYPE ]) {
            case self::ITYPE_DOI_XOR_CE:
                $config = self::ITYPE_DOI_XOR_CE;
                if (empty($instructions[ self::INST_DOI_PERMISSION ]) || empty($instructions[ self::INST_DOIMAILING_KEY ]) || empty($instructions[ self::INST_CONTACT_EVENT_ID ])) {
                    $hasErrors = true;
                    $message = "you configured {$config}. Please provide values for " . self::INST_DOI_PERMISSION . ", " . self::INST_DOIMAILING_KEY . ", " . self::INST_CONTACT_EVENT_ID;
                    $runIntegration = '';
                } elseif (!empty($instructions[ self::INST_DOI_PERMISSION ]) && !empty($instructions[ self::INST_DOIMAILING_KEY ]) && !empty($instructions[ self::INST_CONTACT_EVENT_ID ])) {
                    $hasErrors = false;
                    $message = "run {$config}";
                    $runIntegration = $config;
                }
                break;
            case self::ITYPE_DOI:
                $config = self::ITYPE_DOI;
                if (empty($instructions[ self::INST_DOI_PERMISSION ]) /*|| empty($instructions[ self::INST_DOIMAILING_KEY ])*/) {
                    $hasErrors = true;
                    $message = "you configured {$config}. Please provide values for " . self::INST_DOI_PERMISSION . ", " . self::INST_DOIMAILING_KEY;
                    $runIntegration = '';
                } elseif (!empty($instructions[ self::INST_DOI_PERMISSION ]) /*&& !empty($instructions[ self::INST_DOIMAILING_KEY ])*/) {
                    $hasErrors = false;
                    $message = "run {$config}";
                    $runIntegration = $config;
                }
                break;
            case self::ITYPE_CE:
                $config = self::ITYPE_CE;
                if (empty($instructions[ self::INST_CONTACT_EVENT_ID ])) {
                    $hasErrors = true;
                    $message = "you configured {$config}. Please provide value for " . self::INST_CONTACT_EVENT_ID;
                    $runIntegration = '';
                } else {
                    $hasErrors = false;
                    $message = "run {$config}";
                    $runIntegration = $config;
                }
                break;
            case self::ITYPE_MA:
                $config = self::ITYPE_MA;
                if (empty($instructions[ self::INST_MARKETING_AUTOMATION_ID ])) {
                    $hasErrors = true;
                    $message = "you configured {$config}. Please provide value for " . self::INST_MARKETING_AUTOMATION_ID;
                    $runIntegration = '';
                } else {
                    $hasErrors = false;
                    $message = "run {$config}";
                    $runIntegration = $config;
                }
                break;
        }

        return [
            'error' => $hasErrors,
            'message' => $message,
            'runintegration' => $runIntegration,
        ];
    }

    /**
     * @param MioContact &$newContact
     * @param array $contactData
     * @return MioContact
     * @author jsr
     */
    protected function setStandardFields(MioContact &$newContact, array &$contactData)
    {
        $newContact = new MioContact();
        $newContact->standard_fields[ StandardContactFieldsXY::$SALUTATION ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$SALUTATION ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$SALUTATION ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$GENDER ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$GENDER ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$GENDER ]) : $this->getGenderBySalutation($newContact->standard_fields[ StandardContactFieldsXY::$SALUTATION ]);

        $newContact->standard_fields[ StandardContactFieldsXY::$TITLE ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$TITLE ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$TITLE ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$FIRSTNAME ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$FIRSTNAME ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$FIRSTNAME ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$LASTNAME ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$LASTNAME ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$LASTNAME ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$FULLNAME ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$FULLNAME ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$FULLNAME ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$ORGANIZATION ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$ORGANIZATION ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$ORGANIZATION ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$ADDRESS ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$ADDRESS ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$ADDRESS ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$HNR ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$HNR ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$HNR ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$ZIP ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$ZIP ])) ? $this->cleanInt($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$ZIP ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$CITY ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$CITY ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$CITY ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$COUNTRY ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$COUNTRY ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$COUNTRY ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$REGION ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$REGION ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$REGION ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$STATE ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$STATE ])) ? $this->cleanString($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$STATE ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$BIRTHDAY ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$BIRTHDAY ])) ? $this->cleanDate($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$BIRTHDAY ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$NAMEDAY ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$NAMEDAY ])) ? $this->cleanDate($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$NAMEDAY ]) : '';

        $newContact->standard_fields[ StandardContactFieldsXY::$LOCALE ] = (!empty($contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$LOCALE ]))
            ? (1 === preg_match('/^([a-z]{2}|[a-z]{2}_[A-Z]{2})$/',
                    $contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$LOCALE ]))
                ? $contactData[ self::STANDARDFIELDS ][ StandardContactFieldsXY::$LOCALE ]
                : ''
            : '';

        foreach ($newContact->standard_fields as $key => $standard_field) {
            if (empty($standard_field)) {
                unset($newContact->standard_fields[ $key ]);
            }
        }
    }

    /**
     * @param string $string
     * @return string
     * @author jsr
     */
    protected function cleanString(string $string): string
    {
        return trim(filter_var($string, FILTER_SANITIZE_STRING));
    }

    /**
     * @param string $salutation
     * @return string $gender {'m'|'f'} Maileon does not allow 'd' (divers)
     * @author jsr
     */
    protected function getGenderBySalutation(string $salutation): string
    {
        $gender = 'm';
        $c_salutation = preg_replace('/\./', '', strtolower($this->cleanString($salutation)));
        switch ($c_salutation) {
            case 'hr';
            case 'herr':
            case 'mr':
                $gender = 'm';
                break;
            case 'fr';
            case 'frau':
            case 'mrs':
            case 'ms':
            case 'miss':
                $gender = 'f';
                break;
            default:
                $gender = 'f';
                break;
        }
        return $gender;
    }

    /**
     * @param string $integer
     * @return string
     * @author jsr
     */
    protected function cleanInt(string $integer): string
    {
        return filter_var(trim($integer), FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * @param string $date
     * @return string
     * @author jsr
     */
    protected function cleanDate(string $date): string
    {
        return date('Y-m-d', strtotime($date));
    }

    /**
     * @param MioContact &$newContact
     * @param array $contactData
     * @author jsr
     */
    protected function setCustomFields(MioContact &$newContact, array &$contactData)
    {
        $contactService = new ContactsService($this->config);
        $contactCustomFields = $this->getContactsCustomFields();
        if (!empty($contactData[ self::CUSTOMFIELDS ])) {
            foreach ($contactData[ self::CUSTOMFIELDS ] as $customFieldName => $customFieldValue) {
                if ($customFieldName === $this->checkCustomFieldName($customFieldName)) {
                    $newContact->custom_fields[ $customFieldName ] = $customFieldValue;
                }
                /*if($customFieldName === 'MIO_smart_tags') {
                    $this->logger->info('MIO_smart_tags '. json_encode($customFieldValue));
                    $im = explode(',',$customFieldValue);
                    $this->logger->info('MIO_smart_tags '. json_encode($im));
                    foreach ($im as $i) {
                        $smartTag = 'MIO_'.$this->sanitizeString($i);
                        if(isset($newContact->custom_fields[ $smartTag ])) {
                            continue;
                        }
                        $this->logger->info('smart_tag '. json_encode($smartTag));
                        if(!isset($contactCustomFields[$smartTag])) {
                            $r = $contactService->createCustomField($smartTag, 'string');
                            $this->logger->info('smart_tag response '. json_encode($r));
                        }
                        $newContact->custom_fields[ $smartTag ] = 'true';
                        $this->logger->info('smart_tags created '. json_encode([$smartTag => $newContact->custom_fields[
                            $smartTag ]]));
                    }
                }*/
                $this->logger->info('CustomFields middle'. json_encode($newContact->custom_fields));
                //$newContact->custom_fields = array_unique($newContact->custom_fields);
            }
        }
        $this->logger->info('CustomFields End'. json_encode($newContact->custom_fields));
    }

    /**
     * @param string $customFieldName
     * @return string $customFieldName, if it fulfills requirements, else empty string ''
     * @author jsr
     */
    protected function checkCustomFieldName(string $customFieldName): string
    {
        $sanitized_name = $this->cleanString($customFieldName);

        if (1 === preg_match('/[a-zA-Z0-9_]{3,}/', $sanitized_name) && $sanitized_name === $customFieldName) {
            return $customFieldName;
        }

        return '';
    }

    /**
     * @param bool|string|int $doiInstruction
     * @return Permission
     * @author jsr
     */
    protected function getDoiPermission($doiInstruction): Permission
    {
        if (2 === $doiInstruction || '2' === $doiInstruction) {
            return Permission::$SOI;
        }

        if (3 === $doiInstruction || '3' === $doiInstruction) {
            return Permission::$COI;
        }

        if (4 === $doiInstruction || '4' === $doiInstruction) {
            return Permission::$DOI;
        }

        if (5 === $doiInstruction || '5' === $doiInstruction) {
            return Permission::$DOI_PLUS;
        }

        return Permission::$NONE;
        //return (1 === $doiInstruction || '1' === $doiInstruction || 'true' === strtolower($doiInstruction) || true === $doiInstruction) ? Permission::$DOI_PLUS : Permission::$NONE;
    }

    /**
     * @param MioContact $contact
     * @param string $email
     * @param string|int $contactEventId
     * @return array
     */
    public function runCE(array $contactEventFields, string $email, int $contactEventId): array
    {
        $this->logger->error("STARTING TRANSACTIONS-SERVICE WITH config = " . json_encode($this->config),
            [__METHOD__, __LINE__]);
        try {
            $transactionsService = new TransactionsService($this->config);
            $transactionsService->setDebug(false);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->ret->set(false, 'n.a', 'Failed to instantiate MIO TransactionsService')->toArray();
        }


        try {
            $transaction = new Transaction();
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $this->ret->set(false, 'n.a', 'Failed to instantiate MIO Transaction')->toArray();
        }
        $transaction->contact = new ContactReference();
        $transaction->contact->email = strtolower($email);
        $transaction->type = (int)$contactEventId;

        foreach ($contactEventFields as $key => $value) {
            if (!empty($key) && !empty($value)) {
                $transaction->content[ $key ] = $value;
            }
        }

        $transactions = array($transaction);

        try {
            $response = $transactionsService->createTransactions($transactions, false, false);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        $this->logger->error("TRANSACTIONS-SERVICE TERMINATED WITH: " . $response->getBodyData(),
            [__METHOD__, __LINE__]);

        $obj = json_decode($response->getBodyData());
        if (isset($obj->reports[ 0 ]->queued)) {
            $this->logger->error("TRANSACTIONS-SERVICE TERMINATED WITH: " . $obj->reports[ 0 ]->queued,
                [__METHOD__, __LINE__]);
        }

        if ($response->isSuccess()) {
            return $this->ret->set($response->isSuccess(), $response->getStatusCode(),
                "ContactEvent {$contactEventId} succcessfully triggered for {$email}")->toArray();

        } else {
            return $this->ret->set($response->isSuccess(), $response->getStatusCode(),
                "ContactEvent {$contactEventId} could not be triggered for {$email}. MIO returned: " . json_encode($response->getBodyData()))->toArray();
        }
    }

    /**
     * @param string $doiKey
     * @return bool
     */
    public function isValidDoiKey(string $doiKey): bool
    {
        $ret = false;
        $list = $this->getMailingsDoiKeys($this->apiKey);
        foreach ($list as $item) {
            if ($item[ 'doiKey' ] === $doiKey) {
                return true;
            }
        }
        return $ret;
    }

    /**
     * @param string $apiKey
     * @return array
     * @author jsr
     */
    public function getMailingsDoiKeys(string $apiKey): array
    {
        if (empty($apiKey)) {
            return [];
        } // early exit


        $doiMailings = [];
        $this->config[ 'API_KEY' ] = $apiKey;

        try {
            $t1 = microtime(true);
            $mailingsService = new MailingsService($this->config);
            $mailingsService->setDebug(false);

            $response = $mailingsService->getMailingsByTypes(array('doi'), ['state', 'name']);
            $t2 = microtime(true);
            $d = $t2 - $t1;
            $res = $response->getResult();

            $this->logger->error("duration MIO->getMailingsByTypes = {$d} seconds", [__METHOD__, __LINE__]);

            $t1 = microtime(true);


            foreach ($res as $result) {
                $doiKeys = $mailingsService->getDoiMailingKey((string)$result->id);
                $mailingNames = $mailingsService->getName((string)$result->id);
                $doiKey = simplexml_load_string($doiKeys->getResult());

                // $this->logger->error('DOI KEY = ' . json_encode($doiKey). ' RESULT = '.json_encode( $result->fields['state']), [__METHOD__,__LINE__] );

                if ($result->fields[ 'state' ] === 'released') { // $possibleStates =  ["draft","released","archiving"];
                    $doiMailing = array(
                        'doiKey' => (string)$doiKey[ 0 ],
                        'name' => (string)$mailingNames->getResult(),
                    );

                    array_push($doiMailings, $doiMailing);
                }
            }
            $t2 = microtime(true);
            $d = $t2 - $t1;
            $this->logger->error("duration FOREACH after MIO->getMailingsByTypes = {$d} seconds",
                [__METHOD__, __LINE__]);

            return $doiMailings;
        } catch (com_maileon_api_MaileonAPIException $e) {
            $err = $e->getMessage();
            return [];
        }
    }

    /**
     * @param MioContact $newContact
     * @param            $contactSource
     * @param            $doi
     * @param            $doiPlus
     * @param            $doiMailingKey
     * @return array
     */
    public function insertOrUpdateContact(
        MioContact $newContact,
        string $contactSource,
        bool $doi,
        bool $doiPlus,
        string $doiMailingKey
    ) {
        $startTime = microtime(true);
        $this->logger->error('IN insertOrUpdateContact', [__METHOD__, __LINE__]);
        try {
            $contactsService = new ContactsService($this->config);
            $contactsService->setDebug(false);
            $response = $contactsService->createContact(// the contact to create or unsetEmailList; if no permission is set, the Maileon default permission "NONE" will be used
                $newContact, // the synchronization mode to employ
                SynchronizationMode::$UPDATE,
                // A string intended to describe the source of the contact. If provided, the string will be stored with the doi process.
                $contactSource,
                // In case where this method was called by a subscription page, this string offers the possibility to keep track of it for use in reports
                '', // DOI
                $doi,
                // This parameter is ignored if doi is not provided or false. In case the doi process succeeds, Maileon will be allowed to track opens and clicks of the contact.
                $doiPlus,
                // This parameter is ignored if doi is not provided or false. References the doi mailing to be used. If not provided, the default doi mailing will be used.
                $doiMailingKey);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }


        $bodyData = $response->getBodyData();
        if ($response->isClientError() && !empty($bodyData)) {
            $message = preg_replace('#<error><message>#', '', $bodyData);
            $message = preg_replace('#</message></error>#', '', $message);
            $message = preg_replace('#\'#', '|', $message);
            $message = "Contact {$newContact->email} not created. " . $message;
            $statusCode = (empty($response->getStatusCode())) ? 'n.a.' : $response->getStatusCode();
        } else {
            $message = ($doi && !empty($doiMailingKey)) ? "Created contact with DOI" : 'Created / updated contact without DOI';
            $statusCode = '200';
        }
        $diff = $startTime - microtime(true);
        $this->logger->info('insertOrUpdateContact '. json_encode($diff));
        return $this->ret->set($response->isSuccess(), $statusCode, $message)->toArray();
    }

    /**
     * @param $email
     * @param $marketingAutomationId
     * @return array
     */
    public function runMA(string $email, int $marketingAutomationId)
    {
        $maService = new MarketingAutomationService($this->config);
        $maService->setDebug(false);

        try {
            $response = $maService->startMarketingAutomationProgram($marketingAutomationId, $email);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        $this->logger->error('MA RESPONSE = ' . $response->getBodyData(), [__METHOD__, __LINE__]);

        if ($response->isSuccess()) {
            return $this->ret->set($response->isSuccess(), $response->getStatusCode(),
                "MA {$marketingAutomationId} successfully triggered for {$email}. MIO response: " . $response->getBodyData())->toArray();
        } else {
            return $this->ret->set(false, $response->getStatusCode(),
                "Failed to trigger MA {$marketingAutomationId} for {$email}. MIO error: " . $response->getResult())->toArray();
        }
    }

    /**
     * @param $email
     * @param $marketingAutomationId
     * @return array
     */
    public function processhookRunMA(string $email, int $marketingAutomationId)
    {
        $maService = new MarketingAutomationService($this->config);
        $maService->setDebug(false);

        try {
            $response = $maService->startMarketingAutomationProgram($marketingAutomationId, $email);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }

        $this->logger->error('MA RESPONSE = ' . $response->getBodyData(), [__METHOD__, __LINE__]);

        if ($response->isSuccess()) {
            $this->ret->set($response->isSuccess(), $response->getStatusCode(),
                "MA {$marketingAutomationId} successfully triggered for {$email}. MIO response: " . $response->getBodyData())->toArray();
            return [
                'status' => 1,
                'error' => '',
                'success' => "MA {$marketingAutomationId} successfully triggered for {$email}. MIO response: " . $response->getResult(),
            ];
        } else {
            $this->ret->set(false, $response->getStatusCode(),
                "Failed to trigger MA {$marketingAutomationId} for {$email}. MIO error: " . $response->getResult())->toArray();

            return [
                'status' => 0,
                'error' => "Failed to trigger MA {$marketingAutomationId} for {$email}. MIO error: " . $response->getResult(),
                'success' => '',
            ];
        }
    }

    public function setDoi(MioContact $newContact, string $doiKey)
    {
        $newContact->permission = Permission::$NONE;
    }

    public function fetchDoiKey(string $doiKeyName): string
    {
        $doikey = '';
        $list = $this->getMailingsDoiKeys($this->apiKey);

        foreach ($list as $item) {
            if ($item[ 'name' ] === $doiKeyName) {
                $doikey = $item[ 'doiKey' ];
            }
        }
        return $doikey;
    }

    /**
     * @param string $apiKey
     * @return bool
     * @author jsr
     */
    public function mioPingCheckGet(string $apiKey): bool
    {
        if ($this->setApiKey($apiKey)) {
            $pingService = new PingService($this->config);
            $pingService->setDebug(false);
            $response = $pingService->pingGet();
            return $response->isSuccess();
        }

        return false;
    }

    /**
     * @param string $apiKey
     * @return bool
     * @author jsr
     */
    public function mioPingCheckPost(string $apiKey): bool
    {
        if ($this->setApiKey($apiKey)) {
            $pingService = new PingService($this->config);
            $pingService->setDebug(false);
            $response = $pingService->pingPost();
            return $response->isSuccess();
        }
        return false;
    }

    /**
     * @param string $customFieldName
     * @param string $datatype
     * @param string $operation
     * @return bool
     * @author Pradeep
     */
    public function synchronizeCustomFields(string $customFieldName, string $operation, string $datatype = ''): bool
    {
        try {
            if (empty($customFieldName) || !$this->isApiKeySet() || !in_array($operation,
                    ['insert', 'unsetEmailList', 'delete'])) {
                throw new RuntimeException('Invalid parameter provided for sychronizing customfields from MIO');
            }
            $standardDataTypes = $this->getStandardDataTypes();
            // if the datatype is not one of the standard datatype, then unsetEmailList default datatype.
            if (!isset($datatype) || !in_array($datatype, $standardDataTypes, true)) {
                $datatype = self::DATATYPE_STRING;
            }
            $apikey = $this->apiKey;
            $this->config[ 'API_KEY' ] = $apikey;
            $contactService = new ContactsService($this->config);
            $contactService->setDebug(false);
            $this->logger->info(json_encode(['operation' => $operation], JSON_THROW_ON_ERROR));
            if ($operation === 'insert' || $operation === 'unsetEmailList') {
                $response = $contactService->createCustomField($customFieldName, $datatype);
                $this->logger->info(json_encode(['insert/unsetEmailList' => $response], JSON_THROW_ON_ERROR));
            }
            if ($operation === 'delete') {
                $response = $contactService->deleteCustomField($customFieldName);
                $this->logger->info(json_encode(['delete' => $response], JSON_THROW_ON_ERROR));
            }
            $this->logger->info(json_encode(['Outside' => $response], JSON_THROW_ON_ERROR));
            if ($response->isClientError()) {
                throw new RuntimeException($response->getBodyData());
            }
            return $response->isSuccess();
        } catch (RuntimeException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return false;
        }
    }

    /**
     * @return string[]|null
     * @author Pradeep
     */
    public function getStandardDataTypes(): array
    {
        return [
            self::DATATYPE_DATE,
            self::DATATYPE_STRING,
            self::DATATYPE_FLOAT,
            self::DATATYPE_DATE,
            self::DATATYPE_BOOLEAN,
        ];
    }

    public function deleteCustomField(string $customFieldName): ?bool
    {
        if (empty($customFieldName) || !$this->isApiKeySet()) {
            throw new RuntimeException('Invalid APIKey or CustomField Name provided');
        }
        $apikey = $this->apiKey;
        $this->config[ 'API_KEY' ] = $apikey;
        $contactService = new ContactsService($this->config);
        $contactService->setDebug(false);
        $response = $contactService->deleteCustomField($customFieldName);
    }

    public function getTransactionTypes(string $apikey): ?array
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://api.maileon.com/1.0/transactions/types',
//			CURLOPT_SSL_VERIFYPEER => false, // make curl request work from local sys
//			CURLOPT_SSL_VERIFYSTATUS => false, // make p curl request work from local sys
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/vnd.maileon.api+xml',
                'Authorization: Basic ' . base64_encode($apikey),
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $obj = simplexml_load_string($response);

        if (false === $obj) {
            return null;
        }

        return json_decode(json_encode($obj), true, 512, JSON_OBJECT_AS_ARRAY);
    }

    protected function cleanCustomFieldName(string $string): string
    {
        return preg_replace('/[^a-z^0-9]+/i/', '', trim(filter_var($string, FILTER_SANITIZE_STRING)));
    }

    /**
     * @param string|bool|int $doiInstruction
     * @return bool
     * @author jsr
     */
    protected function isDoi($doiInstruction = null): bool
    {
        return (1 === $doiInstruction || '1' === $doiInstruction || 'true' === strtolower($doiInstruction) || true === $doiInstruction);
    }

    protected function getContactReference(string $email): ?ContactReference
    {
        $contact = $this->getContactByEmail($email);

        if (!empty($contact)) {
            $contactReference = new ContactReference();
            $contactReference->permission = $this->getPermissionForContact($contact[ 'permission' ]);
            $contactReference->id = $contact[ 'id' ];
            $contactReference->external_id = '';
            return $contactReference;
        } else {
            return null;
        }
    }

    /**
     * @param string $email
     * @return Permission|null null if no email address or contact not found
     * @author jsr
     */
    public function getPermissionForContact(string $email): ?Permission
    {
        if (false === filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return null;
        }

        $contactArr = $this->getContactByEmail($email);

        if (!empty($contactArr[ 'permission' ])) {
            switch ($contactArr[ 'permission' ]) {
                case 1:
                    return Permission::$NONE;
                case 2:
                    return Permission::$SOI;
                case 3:
                    return Permission::$COI;
                case 4:
                    return Permission::$DOI;
                case 5:
                    return Permission::$DOI_PLUS;
                case 6:
                    return Permission::$OTHER;
                default:
                    return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @param int|string $number
     * @return Permission
     * @author jsr
     */
    protected function int2Permission($number): ?Permission
    {
        switch ((int)$number) {
            case 1:
                return Permission::$NONE;
            case 2:
                return Permission::$SOI;
            case 3:
                return Permission::$COI;
            case 4:
                return Permission::$DOI;
            case 5:
                return Permission::$DOI_PLUS;
            case 6:
                return Permission::$OTHER;
            default:
                return null;
        }
    }

    /**
     * @param int|Permission $permission
     * @return bool
     * @author jsr
     */
    protected function hasOptIn($permission): bool
    {
        if ($permission instanceof Permission) {
            $perm = $permission->getCode();
        } elseif (is_int($permission)) {
            $perm = $permission;
        }
        return in_array($perm, [2, 3, 4, 5]);
    }

}
