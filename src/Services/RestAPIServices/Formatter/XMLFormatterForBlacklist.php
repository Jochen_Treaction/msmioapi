<?php

namespace App\Services\RestAPIServices\Formatter;

use SimpleXMLElement;

class XMLFormatterForBlacklist implements AbstractFormatter
{
    public function __construct()
    {
        $this->xml = new SimpleXMLElement('<add_entries_action/>');
    }

    public function format(array $data)
    {
        $importName = $data['import_name'];
        $emails = $data['emails'];
        $this->xml->addChild('import_name',$importName);
        $entries = $this->xml->addChild('entries');
        foreach($emails as $email) {
            $entries->addChild('entry', $email);
        }
        return $this->xml->asXML();
    }

    public function getBlacklist():?array
    {

    }

    public function getBlacklists():array
    {

    }
}