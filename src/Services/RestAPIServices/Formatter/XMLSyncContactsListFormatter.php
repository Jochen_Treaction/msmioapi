<?php

namespace App\Services\RestAPIServices\Formatter;

use App\Services\RestAPIServices\Formatter\AbstractFormatter;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;

class XMLSyncContactsListFormatter implements AbstractFormatter
{
    /**
     * @var \array[][]
     * @author Pradeep
     * @internal Example format of payload in array,  But the sync contacts will always consider in xml format
     */
    private $syncContactFormat = array(
        'contacts' =>
            array(
                0 =>
                    array(
                        'email' => 'max.mustermann@xqueue.com',
                        'standard_fields' =>
                            array(
                                0 =>
                                    array(
                                        'name' => 'LASTNAME',
                                        'value' => 'Mustermann',
                                    ),
                                1 =>
                                    array(
                                        'name' => 'FIRSTNAME',
                                        'value' => 'Max',
                                    ),
                            ),
                        'custom_fields' =>
                            array(
                                0 =>
                                    array(
                                        'name' => 'MIO_asdf',
                                        'value' => 'Mustermann',
                                    ),
                                1 =>
                                    array(
                                        'name' => 'testFromAPI',
                                        'value' => 'Max',
                                    ),
                            ),
                    ),
                1 =>
                    array(
                        'email' => 'erika.mustermann@xqueue.com',
                        'external_id' => 'external-id-2',
                        'standard_fields' =>
                            array(
                                0 =>
                                    array(
                                        'name' => 'LASTNAME',
                                        'value' => 'Mustermann',
                                    ),
                                1 =>
                                    array(
                                        'name' => 'FIRSTNAME',
                                        'value' => 'Erika',
                                    ),
                            ),

                        'custom_fields' =>
                            array(
                                0 =>
                                    array(
                                        'name' => 'MIO_asdf',
                                        'value' => 'Mustermann',
                                    ),
                                1 =>
                                    array(
                                        'name' => 'testFromAPI',
                                        'value' => 'Max',
                                    ),
                            ),
                    ),
            ),
    );

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;

    }

    public function format(array $data)
    {
        $this->xml = new SimpleXMLElement('<contacts/>',);
        $contacts = $data;
        foreach ($contacts as $c) {

            if (empty($this->xml->getName())  || ($this->xml->getName()==='')) {
                $contactsXML = $this->xml->addChild('contacts');
                $contact = $contactsXML->addChild('contact');
            }else {
                $contact = $this->xml->addChild('contact');
            }


            $contact->addChild('email', $this->sanitizeString($c[ 'email' ]));
            $sdFields = $contact->addChild('standard_fields');
            foreach ($c[ 'standard_fields' ] as $sdField) {
                $field = $sdFields->addChild('field');
                $field->addChild('name', $sdField[ 'name' ]);
                $field->addChild('value', $sdField[ 'value' ]);
            }
            if(isset($c[ 'custom_fields' ])) {
                $cusFields = $contact->addChild('custom_fields');
                foreach ($c[ 'custom_fields' ] as $cusField) {
                    $value ='';
                    $field = $cusFields->addChild('field');
                    $field->addChild('name', $cusField[ 'name' ]);
                    if(!empty($cusField[ 'value' ])) {
                        $value = $this->sanitizeString($cusField[ 'value' ]);
                    }
                    $field->addChild('value', $value);
                }
            }
        }


        return $this->xml->asXML();

    }


    /**
     * Helper function to remove German ümlauts.
     * Since the Xqueue is not supporting the special chracters in the emails.
     *
     * - Check the API once again, if the Xqueue supports special characters ,
     * just remove this function
     * @param string $string
     * @return string
     * @author Pradeep
     */
    private function sanitizeString(string $string): string
    {
        // Sanitize string
        $sanitized_string = filter_var($string, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
        // Replace German Characters
        $sanitized_string = preg_replace(['/Ä/', '/ä/', '/Ö/', '/ö/', '/ß/', '/Ü/', '/ü/', '/á/','/é/','/È/'],
            ['AE', 'ae', 'OE', 'oe', 'ss', 'UE', 'ue', 'a','e','E'], $sanitized_string);
        // Replace '&' character
        return str_replace(array(
            '&',
            '/',
            '!',
            '"',
            '§',
            '$',
            '%',
            '#',
            '*',
            '+',
            ';',
            ',',
            '>',
            '<',
            '|',
        ),
            array('-', '-', '', '', '', '', '', '', '', '', '', '', '', '', ''), $sanitized_string);
    }

}