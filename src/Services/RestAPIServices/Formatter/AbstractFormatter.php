<?php

namespace App\Services\RestAPIServices\Formatter;

interface AbstractFormatter
{

    public function format(array $data);
}