<?php

namespace App\Services\RestAPIServices;

class Authorization extends MailOnRestAPI
{
    public function getAuthorizationKey(string $apikey): ?string
    {
        if(empty($apikey)) {
            return null;
        }
        return 'Basic '.base64_encode($apikey);
    }
}