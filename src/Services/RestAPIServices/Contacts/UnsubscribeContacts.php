<?php

namespace App\Services\RestAPIServices\Contacts;

use App\Services\RestAPIServices\Authorization;
use App\Services\RestAPIServices\MailOnRestAPI;
use App\Services\RestAPIServices\SendRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class UnsubscribeContacts extends MailOnRestAPI
{

    /**
     * @var SendRequest
     * @author Pradeep
     */
    private $sendRequest;
    /**
     * @var Authorization
     * @author Pradeep
     */
    private $authorization;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;

    public CONST Reason_Too_Many_Messages = "too_many_messages";
    public CONST Reason_No_Demand = "no_demand";
    public CONST Reason_Change_Of_Channel = "change_of_channel";
    public CONST Reason_Change_Of_Interest = "change_of_interest";
    public CONST Reason_Technical_Reasons = "technical_reasons";
    public CONST Reason_Other = "other";

    private CONST METHOD = "DELETE";
    private CONST ENDPOINT = "https://api.maileon.com/1.0/contacts/email/%s/unsubscribe?reason=%s";



    public function __construct(
        SendRequest $sendRequest,
        Authorization $authorization,
        LoggerInterface $logger)
    {
        $this->sendRequest = $sendRequest;
        $this->authorization = $authorization;
        $this->logger = $logger;
    }

    /**
     * @param string $apikey
     * @param string $email
     * @param string $reason
     * @return int
     * @author Pradeep
     */
    public function unsubscribe(string $apikey, string $email, string $reason = self::Reason_Other):int
    {
        if (empty($apikey) ||empty($email)) {
            return Response::HTTP_BAD_REQUEST;
        }
        $endPoint = sprintf(self::ENDPOINT, $email, $reason);
        $this->logger->info('unsubscribe endpoint: ' . $endPoint);

        $properties[ 'Authorization' ] = $this->authorization->getAuthorizationKey($apikey);
        $properties[ 'Content-Type' ] = 'application/vnd.maileon.api+xml';
        $properties[ 'Accept' ] = 'application/vnd.maileon.api+xml';

        $this->logger->info('Properties '. json_encode($properties));
        return $this->sendRequest->send(self::METHOD,'',$endPoint, $properties);
    }
}