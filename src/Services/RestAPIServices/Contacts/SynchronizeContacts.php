<?php

namespace App\Services\RestAPIServices\Contacts;

use App\Services\RestAPIServices\Authorization;
use App\Services\RestAPIServices\Contacts\ContactList\ContactList;
use App\Services\RestAPIServices\Contacts\ContactList\EmailList;
use App\Services\RestAPIServices\Formatter\XMLSyncContactsListFormatter;
use App\Services\RestAPIServices\MailOnRestAPI;
use App\Services\RestAPIServices\SendRequest;
use com_maileon_api_contacts_Permission as Permission;
use com_maileon_api_contacts_SynchronizationMode as SynchronizationMode;

use Psr\Log\LoggerInterface;

class SynchronizeContacts extends MailOnRestAPI
{
    private const METHOD = 'POST';
    private const ENDPOINT = 'https://api.maileon.com/1.0/contacts';
    /**
     * @var XMLSyncContactsListFormatter
     * @author Pradeep
     */
    private $formatter;
    /**
     * @var SendRequest
     * @author Pradeep
     */
    private $sendRequest;
    /**
     * @var Authorization
     * @author Pradeep
     */
    private $authorization;
    /**
     * @var ContactList
     * @author Pradeep
     */
    private $contactList;
    /**
     * @var EmailList
     * @author Pradeep
     */
    private $emailList;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var int
     * @author Pradeep
     */
    private $syncMode;
    /**
     * @var int
     * @author Pradeep
     */
    private $permission;

    public function __construct(
        XMLSyncContactsListFormatter $xmlFormatter,
        SendRequest $sendRequest,
        Authorization $authorization,
        ContactList $contactList,
        EmailList $emailList,
        LoggerInterface $logger
    ) {
        $this->formatter = $xmlFormatter;
        $this->sendRequest = $sendRequest;
        $this->authorization = $authorization;
        $this->contactList = $contactList;
        $this->emailList = $emailList;
        $this->logger = $logger;
    }

    /**
     * @param string $apikey
     * @param array $contacts
     * @return bool|null
     * @author Pradeep
     */
    public function sync(
        string $apikey,
        array $contacts
    ) {
        $this->logger->info('sync:started');
        if (empty($apikey) || empty($contacts)) {
            return null;
        }

        if(!$this->setAPIKey($apikey)) {
            return null;
        }

        // Call recursive function to filter unique contacts and send to eMIO.
        $this->logger->info('sync:recursiveFilterDuplicateAndSyncChunk');
        return  $this->recursiveFilterDuplicateAndSyncChunk($contacts);
    }


    /**
     * recursiveFilterDuplicateAndSyncChunk is a recursive function
     * - fetches the uniqueContacts for each recursive and synchronizes with eMIO.
     * @param array $contactList
     * @return bool
     * @author Pradeep
     */
    private function recursiveFilterDuplicateAndSyncChunk(array $contactList): bool
    {
        $this->logger->info('recursiveFilterDuplicateAndSyncChunk:started');
        // check for empty contacts.
        if (empty($contactList)) {
            $this->logger->info('recursiveFilterDuplicateAndSyncChunk:ended:emptyContactList');
            return false;
        }
        $this->contactList->set($contactList);
        $this->logger->info('$contactList count '.json_encode(count($contactList[0])));
        $uniqueEmailList = $this->emailList->generateUniqueEmail($contactList);

        $this->logger->info('UniqueEmailList count '.json_encode(count($uniqueEmailList)));
        //$this->logger->info('UniqueEmailList '.json_encode($uniqueEmailList));
        $uniqueContacts = $this->contactList->getContactsForEmailList($uniqueEmailList);
        $this->logger->info('$uniqueContacts count '.json_encode(count($uniqueContacts)));
        if (count($uniqueContacts) > 0) {
            $this->syncChunk($uniqueContacts);
        }
        if (!$this->contactList->unsetEmailList($uniqueEmailList)) {
            $this->logger->info('recursiveFilterDuplicateAndSyncChunk:ended:false');
            return false;
        }
        $remainingContacts = $this->contactList->get();
        if (count($remainingContacts) > 0) {
            $this->recursiveFilterDuplicateAndSyncChunk($remainingContacts);
        }
        $this->logger->info('recursiveFilterDuplicateAndSyncChunk:ended');
        return true;
    }

    /**
     * syncChunk synchronizes contact chunk with eMIO using RestAPI.
     * @param $contacts
     * @return void
     * @author Pradeep
     */
    private function syncChunk($contacts): void
    {
        $this->logger->info('syncChunk:started');
        $apikey = $this->getAPIKey();

        if(!empty($apikey)){
            //$this->logger->info('syncChunk $contacts ' . json_encode(['permission'=>Permission::$DOI_PLUS->getCode(), 'sync'=>SynchronizationMode::$UPDATE->getCode()]));
            $payload = $this->formatter->format($contacts);
            //$this->logger->info('XML Payload ' . $payload);
            $contactPreferences = 'permission=' . Permission::$DOI_PLUS->getCode() .
                                  '&sync_mode=' . SynchronizationMode::$UPDATE->getCode().
                                  '&ignore_invalid_contacts=true';

            $endPoint = self::ENDPOINT . '?' . $contactPreferences;
            $properties[ 'Authorization' ] = $this->authorization->getAuthorizationKey($apikey);
            $properties[ 'Content-Type' ] = 'application/vnd.maileon.api+xml; charset=utf-8';
            $logData = [
                // 'payload' => base64_encode($payload),
                'endPoint' => $endPoint,
                'properties' => $properties,
            ];
/*            $this->logger->info('sync contacts' . json_encode($logData));
            $this->logger->info('syncChunk:ended');*/
            $this->sendRequest->send(self::METHOD, $payload, $endPoint, $properties);
        }
    }
}