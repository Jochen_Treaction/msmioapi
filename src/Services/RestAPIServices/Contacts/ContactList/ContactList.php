<?php

namespace App\Services\RestAPIServices\Contacts\ContactList;

class ContactList
{

    /**
     * @var
     * @author Pradeep
     */
    private $contactList;

    /**
     * set the contactList.
     * @param $contactList
     * @return bool
     * @author Pradeep
     */
    public function set($contactList)
    {
        if(!empty($contactList)) {
            $this->contactList = $contactList;
            return true;
        }
        return false;
    }

    /**
     * get fetches the contactList
     * @return mixed
     * @author Pradeep
     */
    public function get()
    {
        return $this->contactList;
    }

    /**
     * unsetEmailList updates the contactList by removing the emailList.
     * - emailList contains the list of emails in key value pair, Key is the index and value is the email.
     * - uses unset to remove the index from contactList.
     * @param array $emailList - contains the list of emails in key value pair, Key is the index and value is the email
     * @return bool
     * @author Pradeep
     */
    public function unsetEmailList(array $emailList): bool
    {
        $contactList = $this->get();
        if( empty($emailList) || empty($contactList)) {
            return false;
        }
        foreach ($emailList as $index => $uniqueEmail) {
            unset($contactList[ $index ]);
        }
        return $this->set(array_values($contactList));
    }

    /**
     * getContactsForEmailList fetches the contacts for the given email list.
     * - collects the contacts based on the index for the emailList.
     * @param array $emailList contains the list of emails in key value pair, Key is the index and value is the email
     * @return array
     * @author Pradeep
     */
    public function getContactsForEmailList(array $emailList ): array
    {
        $uniqueContacts = [];
        $contactList = $this->get();
        if (empty($emailList) || empty($contactList)) {
            return $uniqueContacts;
        }
        foreach ($emailList as $index => $email) {
            if (empty($email)) {
                continue;
            }
            $uniqueContacts[] = $contactList[ $index ];
        }
        return $uniqueContacts;
    }
}