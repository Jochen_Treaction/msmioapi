<?php

namespace App\Services\RestAPIServices\Contacts\ContactList;

class EmailList
{

    public function __construct()
    {

    }

    /**
     * generateUniqueEmail fetches the unique EMails from the given array of contacts.
     * - Loops all the contacts and fetches the email for each contact.
     * - Uses 'array_unique' functionality to get the unique contacts from the collected list of emails.
     * - returns an array which has key value pair, value being unique email,
     *   key being the index at which email is found in contacts array.
     * @param array $contactList
     * @return array
     * @author Pradeep
     */
    public function generateUniqueEmail(array $contactList): array
    {
        $emails = [];
        if (empty($contactList)) {
            return $emails;
        }
        foreach ($contactList as $contact) {
            if(empty($contact) || !isset($contact['email'])) {
                continue;
            }
            $emails[] = $contact[ 'email' ];
        }
        if (empty($emails)) {
            return $emails;
        }
        return array_unique($emails);
    }

    /**
     * set the emailList
     * @param array $emailList
     * @author Pradeep
     */
    public function set(array $emailList)
    {
        $this->emailList = $emailList;
    }

    /**
     * get fetches the emailList.
     * @return array
     * @author Pradeep
     */
    public function get()
    {
        return $this->emailList;
    }
}