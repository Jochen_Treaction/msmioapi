<?php

namespace App\Services\RestAPIServices;

use App\Services\WebProtocolServices\WebProtocolInterface;

class MailOnRestAPI
{
    /**
     * @var WebProtocolInterface
     * @author Pradeep
     */
    private $webProtocol;
    /**
     * @var
     * @author Pradeep
     */
    public $apikey;

    protected const HTTP_BAD_REQUEST = 400;

    public function setAPIKey(string $apikey): bool
    {
        if(!empty($apikey)) {
            $this->apikey = $apikey;
            return true;
        }
        return false;
    }

    protected function getAPIKey(): string
    {
        return $this->apikey;
    }

}