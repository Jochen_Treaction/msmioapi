<?php

namespace App\Services\RestAPIServices\Blacklists;

use App\Services\RestAPIServices\Authorization;
use App\Services\RestAPIServices\Formatter\XMLFormatterForBlacklist;
use App\Services\RestAPIServices\MailOnRestAPI;
use App\Services\RestAPIServices\SendRequest;
use Psr\Log\LoggerInterface;

class Blacklist extends MailOnRestAPI
{

    private const METHOD = 'POST';
    private const ENDPOINT = 'https://api.maileon.com/1.0/blacklists/%s/actions';
    /**
     * @var XMLFormatterForBlacklist
     * @author Pradeep
     */
    private $xmlFormatter;
    /**
     * @var LoggerInterface
     * @author Pradeep
     */
    private $logger;
    /**
     * @var SendRequest
     * @author Pradeep
     */
    private $sendRequest;
    /**
     * @var Authorization
     * @author Pradeep
     */
    private $authorization;

    public function __construct(
        LoggerInterface $logger,
        XMLFormatterForBlacklist $xmlFormatter,
        SendRequest $sendRequest,
        Authorization $authorization
    ) {
        $this->logger = $logger;
        $this->xmlFormatter = $xmlFormatter;
        $this->sendRequest = $sendRequest;
        $this->authorization = $authorization;
    }

    /**
     * @param int $blacklistId
     * @param string $importName
     * @param array $emails
     * @return int
     * @author Pradeep
     */
    public function AddEntriesToBlacklist(string $apikey, int $blacklistId, string $importName, array $emails): int
    {

        if (empty($importName) || empty($emails)) {
            return self::HTTP_BAD_REQUEST;
        }

        $payload = $this->xmlFormatter->format(['import_name' => $importName, 'emails' => $emails]);
        $endPoint = sprintf(self::ENDPOINT, $blacklistId);

        $properties[ 'Authorization' ] = $this->authorization->getAuthorizationKey($apikey);
        $properties[ 'Content-Type' ] = 'application/vnd.maileon.api+xml';

        return $this->sendRequest->send(self::METHOD, $payload, $endPoint, $properties);
    }

    public function getBlacklist()
    {

    }

    public function getBlacklists()
    {

    }
}