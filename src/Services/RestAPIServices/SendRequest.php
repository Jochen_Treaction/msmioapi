<?php

namespace App\Services\RestAPIServices;

use App\Services\WebProtocolServices\HttpClientService;

class SendRequest extends MailOnRestAPI
{

    public function __construct(HttpClientService $httpClient)
    {
        $this->webProtocol = $httpClient;
    }

    public function send(string $method, string $payload, string $endPoint, array $properties = []):int
    {
        return $this->webProtocol->send($method, $payload, $endPoint, $properties);
    }
}